import { useEffect, useState } from "react";
import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import { SignupModelHelper } from "../data/SignupModel";

export default function CursosInicio(props: any) {

    const [typed, setTyped] = useState(false);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('cursos-inicio', 'cursos'));
    }, [])

    function setCourseAlready(already: boolean) {

        if (already === true) {
            document.location = '#/cursos-tipo';
        }
        if (already === false) {
            document.location = '#/cursos-possui-nao';
        }
    }

    let objOptionsList: any = [
        { label: "Sim, já tenho", value: true },
        { label: "Ainda não", value: false }
    ];

    let objJobs = objOptionsList.map((c: any) => {
        return <div className="professional-item" key={objOptionsList.indexOf(c)}>
            <button className="btn btn-outline" onClick={() => setCourseAlready(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={1} total={10} title="Cursos" previous="#/escolaridade-inicio"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString(`E cursos?<br/>Você já fez algum?`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {typed &&
                                    <div className="professional-list">
                                        {
                                            objJobs.map((objCity: any) => {
                                                return objCity;
                                            })
                                        }
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
