import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import SurveyServices from "../services/SurveyServices";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";

export default function PerguntasMotoristaVeiculoNaoPossuo(props: any) {

    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [vehicle, setVehicle] = useState('');

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('veiculo-naopossuo', 'perguntas-especificas-motorista'));

        const signup = SignupModelHelper.getOngoingDriverQuestions();
        if (signup) {
            setVehicle(signup.vehicle);
        } else {
            document.location = './#/perguntas-motorista-veiculo';
        }
    }, [])

    function saveQuestions() {
        SignupModelHelper.updateDriverQuestions({ saved: true });

        const signup = SignupModelHelper.get();
        if (signup) {
            SurveyServices.saveDriverQuestion(signup).then((ret) => {
                console.log( 'saveDriverQuestions', ret );
                if (!ret.success) {
                    setTimeout(() => {
                        setInteractions([...interactions, {
                            author: InteractionAuthor.AMARELINHO,
                            text: ret.message
                        }]);
                        setTimeout(() => {
                            let chat = document.getElementsByClassName('chat')[0];
                            chat.scrollTo(0, chat.scrollHeight);
                        }, 500);
                    }, 1000)
                    
                } else {
                    SignupModelHelper.update({ driverQuestions: null });
                    TagManager.dataLayer(dataLayerHelper.formEnviado('form_perguntas-especificas-motorista'));
                    document.location = '#/experiencias-profissionais';
                }
            }).catch((ex) => {
                setTimeout(() => {
                    setInteractions([...interactions, {
                        author: InteractionAuthor.AMARELINHO,
                        text: ex
                    }]);
                    setTimeout(() => {
                        let chat = document.getElementsByClassName('chat')[0];
                        chat.scrollTo(0, chat.scrollHeight);
                    }, 500);
                }, 1000)
            });
        }
    }

    return (
        <>
            <Header step={7} total={7} title="Motoristas ou Motoboys" previous="#/perguntas-motorista-veiculo"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{'Não possuo'}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`Sem problemas.`)
                                                .start();
                                        }} />
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">                                                    
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }

                            </div>
                        </div>
                    </div>
                </div>
                
                <footer className="footer-fixed">
                    <div className="container">
                        <div className="row d-flex align-items-center justify-content-center">
                            <div className="col-12 col-md-4">
                                <button className="btn btn-secondary btn-full" onClick={() => saveQuestions() }>Continuar</button>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>

        </>);
}
