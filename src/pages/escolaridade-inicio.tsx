import { useEffect, useState } from "react";
import Header from "../components/header";
import { GraduationType, SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import PEXServices from "../services/PEXServices";

export default function EscolaridadeInicio(props: any) {

    const [typed, setTyped] = useState(false);

    useEffect( ()=> {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('formacao', 'escolaridade'));
    }, [])

    function setGraduationType(g: GraduationType) {
        SignupModelHelper.updateGraduation({ type: g })

        if (g === GraduationType.SEM_FORMACAO) {
            const signup = SignupModelHelper.get();
            if (signup) {
                TagManager.dataLayer(dataLayerHelper.formEnviado('form_escolaridade'));

                PEXServices.saveGraduation(signup).then(() => {
                    document.location = '#/cursos-inicio';
                });
            }
        } else {
            document.location = '#/escolaridade-instituicao';
        }

    }

    return (
        <>
            <Header step={1} total={8} title="Escolaridade" previous="#/experiencias-profissionais"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Prontinho!
                                                    Agora me fale um pouco sobre <strong>os seus estudos</strong>.`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {typed &&
                                    <div className="professional-list">
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setGraduationType(GraduationType.SEM_FORMACAO)}>SEM FORMAÇÃO</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setGraduationType(GraduationType.FUNDAMENTAL_INCOMPLETO)}>ENSINO FUNDAMENTAL INCOMPLETO</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setGraduationType(GraduationType.FUNDAMENTAL_COMPLETO)}>ENSINO FUNDAMENTAL COMPLETO</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setGraduationType(GraduationType.MEDIO_INCOMPLETO)}>ENSINO MÉDIO INCOMPLETO</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setGraduationType(GraduationType.MEDIO_COMPLETO)}>ENSINO MÉDIO COMPLETO</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setGraduationType(GraduationType.SUPERIOR_INCOMPLETO)}>ENSINO SUPERIOR INCOMPLETO</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setGraduationType(GraduationType.SUPERIOR_COMPLETO)}>ENSINO SUPERIOR COMPLETO</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setGraduationType(GraduationType.TECNICO)}>ENSINO TÉCNICO</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setGraduationType(GraduationType.POS_GRADUACAO)}>PÓS GRADUAÇÃO</button>
                                        </div>
                                    </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
