import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import { SignupModelHelper } from "../data/SignupModel";

export default function CursosPossuiNao(props: any) {

    const [typed, setTyped] = useState(false);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('possui-nao', 'cursos'));
    }, [])

    return (
        <>
            <Header step={10} total={10} title="Cursos" previous="#/cursos-inicio" finished={false}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>Ainda não</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(5)
                                                    .typeString(`Tudo bem, mas você precisa buscar cursos na sua área. Eles farão a diferença na hora que a empresa estiver vendo o seu currículo.`)
                                                    .pauseFor(1000)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                            </div>
                        </div>

                        {typed &&
                            <footer className="footer-fixed">
                                <div className="container">
                                    <div className="row d-flex align-items-center justify-content-center">
                                        <div className="col-12 col-md-4">
                                            <button className="btn btn-secondary btn-full" onClick={() => document.location = '#/idiomas-inicio'}>Continuar</button>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        }
                    </div>
                </div>
            </div>
        </>);
}
