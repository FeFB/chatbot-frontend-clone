import { useEffect, useState } from "react";
import Header from "../components/header";
import { LanguageLevel, LanguageModel, SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function IdiomasNivel(props: any) {

    const [typed, setTyped] = useState(false);
    const [language, setLanguage] = useState<LanguageModel | null>(null);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('nivel', 'idiomas'));
        setLanguage(SignupModelHelper.getOngoingLanguage());
    }, [])

    function setLanguageLevel(level: LanguageLevel | null) {
        SignupModelHelper.updateLanguage({ level });
        document.location = '#/idiomas-adicionar-outro';
    }

    const arrLanguageTypes = [
        "Inglês",
        "Espanhol",
        "Francês",
        "Alemão",
        "Outro",
    ];

    return (
        <>
            <Header step={3} total={4} title="Idiomas" previous="#/idiomas-inicio"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        {language && language.type !== 4 && arrLanguageTypes[language.type]}
                                        {language && language.type === 4 && language.otherType}
                                    </div>
                                </div>


                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString(`E qual <strong>seu nível de entendimento</strong>?`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {typed &&
                                    <div className="gender-list">
                                        <div className="gender-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguageLevel(LanguageLevel.BASICO)}>Básico</button>
                                        </div>
                                        <div className="gender-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguageLevel(LanguageLevel.INTERMEDIARIO)}>Intermediário</button>
                                        </div>
                                        <div className="gender-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguageLevel(LanguageLevel.FLUENTE)}>Fluente</button>
                                        </div>
                                        <div className="gender-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguageLevel(LanguageLevel.NATIVO)}>Nativo</button>
                                        </div>
                                    </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
