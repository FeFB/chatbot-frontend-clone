import { useEffect } from "react";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function Login(props: any) {

    useEffect( () => {
        TagManager.dataLayer(dataLayerHelper.pagina('login', 'cadastro-usuario'));
    }, [])

    return (
        <>
            <header className="header">
                <div className="container">
                    <div className="row">
                        <div className="col-12 d-flex align-items-center justify-content-center">
                            <div className="header-logo">
                                <a>
                                    <img src="images/logo.svg" alt="O Amarelinho" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div className="login">
                <div className="container">
                    <div className="row d-flex align-items-center justify-content-center">
                        <div className="col-12 col-md-5">

                            <p className="text-center pt-4">Cadastre-se no O Amarelinho</p>

                            <div className="login-list">
                                <div className="login-item">
                                    <button className="btn btn-full btn-primary" onClick={() => document.location = '#/nome'}>@ COM SEU E-MAIL</button>
                                </div>
                                <div className="login-item">
                                    <button className="btn btn-full btn-facebook"><svg width="11" height="20" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 0H8C6.67392 0 5.40215 0.526784 4.46447 1.46447C3.52678 2.40215 3 3.67392 3 5V8H0V12H3V20H7V12H10L11 8H7V5C7 4.73478 7.10536 4.48043 7.29289 4.29289C7.48043 4.10536 7.73478 4 8 4H11V0Z" /></svg> com o Facebook</button>
                                </div>
                                <div className="login-item">
                                    <button className="btn btn-full btn-google"><img src="images/ico-button-google.svg" alt="Google" /> com o Google</button>
                                </div>
                                <div className="login-item">
                                    <button className="btn btn-full btn-outlook"><img src="images/ico-outlook.svg" alt="Outlook" /> com o Outlook</button>
                                </div>
                            </div>

                            <p className="text-center pt-4">Já tem cadastro?</p>

                            <button className="btn btn-full btn-outline" onClick={() => document.location = '/login.aspx'}>Entrar</button>

                        </div>
                    </div>
                </div>
            </div>
        </>);
}
