import { useState, useEffect } from "react";
import Header from "../components/header";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import { SignupModel, SignupModelHelper } from "../data/SignupModel";
import PEXService from "../services/PEXServices";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function Termos(props: any) {

    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [signup, setSignup] = useState<SignupModel | null>(null);

    useEffect(() => {
        TagManager.dataLayer(dataLayerHelper.pagina('termos', 'cadastro-usuario'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setSignup(signup);
        } else {
            document.location = '#/';
        }
    }, [])

    function openTerms() {
        var menu = document.querySelector('.modal');
        menu?.classList.toggle('opened');
    }

    async function createAccount() {
        const objBtn = document.getElementById("btn-criar-conta");

        if (document.getElementById('btn-criar-conta')?.classList.contains('btn-disabled')) return;

        if (objBtn) {
            objBtn.setAttribute('disabled', '');
        }

        const chat = [...interactions, {
            text: 'Criar conta',
            author: InteractionAuthor.CLIENTE
        }];
        setInteractions(chat);

        if (signup) {
            const ret = await PEXService.saveAccount(signup);

            if (!ret.success) {
                setTimeout(() => {
                    setInteractions([...chat, {
                        author: InteractionAuthor.AMARELINHO,
                        text: ret.message
                    }]);
                    setTimeout(() => {
                        let chat = document.getElementsByClassName('chat')[0];
                        chat.scrollTo(0, chat.scrollHeight);
                    }, 500);
                }, 1000)
            } else {
                await SignupModelHelper.checkLogin();
                TagManager.dataLayer(dataLayerHelper.formEnviado('form_cadastro-usuario'));
                document.location = '#/finalizado';
            }
        }

        if (objBtn) {
            objBtn.removeAttribute('disabled');
        }
    }

    return (
        <>
            <Header step={16} total={17} title="Termos" previous="#/senha"></Header>

            <div className="chat-container">

                <div className="chat chat-termos">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p><i>Senha registrada com sucesso</i></p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <p>Para criar sua conta com a gente, você precisa concordar com nossos <a onClick={() => openTerms()} style={{ cursor: "hand" }}>Termos de Uso</a>.</p>
                                        <p>Ele foi atualizado para se adequar à Lei Geral de Proteção de Dados.</p>
                                    </div>
                                </div>

                                <div className="chat-item chat-item-top">
                                    <div className="chat-item-content">
                                        <label className="form-checkbox">
                                            Li e concordo com os Termos de Uso
                                            <input type="checkbox" onClick={() => document.getElementById('btn-criar-conta')?.classList.toggle('btn-disabled')} />
                                            <span className="checkmark"></span>
                                        </label>
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let objRet;
                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <p>
                                                        <Typewriter
                                                            onInit={(typewriter) => {
                                                                typewriter.changeDelay(15).typeString(interaction.text)
                                                                    .start();
                                                            }} />
                                                    </p>
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }


                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-fixed">
                    <div className="container">
                        <div className="row d-flex align-items-center justify-content-center">
                            <div className="col-12 col-md-4">
                                <button id="btn-criar-conta" className="btn btn-secondary btn-full btn-disabled" onClick={() => createAccount()}>criar conta</button>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>


            <div className="modal modal-termos">
                <a className="modal-close" onClick={() => openTerms()}><img src="images/ico-close-modal.svg" alt="Close Modal" /></a>
                <div className="modal-content">
                    <div className="mdl-card content-card">
                        <div id="page_1">
                            <p className="p0 ft0">TERMOS DE USO E POLÍTICA DE DADOS DO SITE O AMARELINHO</p>
                            <p className="p1 ft1">
                                Estes Termos e Condições Gerais
                                <span style={{ whiteSpace: 'nowrap' }}>aplicam-se</span>&nbsp;
                                ao uso dos serviços oferecidos pela
                            </p>
                            <p className="p2 ft2">
                                FLAMBOYANT COMUNICAÇÕES LTDA., pelo site
                                <a href="https://www.oamarelinho.com.br">https://www.oamarelinho.com.br</a> na Internet e todos os websites pertencentes à empresa acima, pessoa jurídica de direito privado, devidamente inscrita no CNPJ/MF sob o nº
                                <span style={{ whiteSpace: 'nowrap' }}>07.251.264/0001-67,</span>&nbsp;
                                com endereço na Avenida Andrômeda, nº 885, Conjunto nº 2102, Alphaville, na cidade de Barueri, Estado de São Paulo, CEP:
                                <span style={{ whiteSpace: 'nowrap' }}>06473-000,</span>&nbsp;
                                Estado de São Paulo, doravante denominada de O Amarelinho®.
                            </p>
                            <p className="p3 ft2">
                                Os CANDIDATOS obrigatoriamente deverão ler, analisar,
                                <span style={{ whiteSpace: 'nowrap' }}>certificar-se</span>
                                de haver entendido e aceitar todas as condições estabelecidas nos Termos e Condições Gerais e nas Políticas de Privacidade, aqui estabelecidas, antes de seu cadastro como usuário do
                                <span className="ft3">O Amarelinho®</span>, sem qualquer exceção.
                            </p>
                            <p className="p4 ft0">1. CONSENTIMENTO</p>
                            <p className="p2 ft2">
                                <span className="ft1">1.1.</span>
                                <span className="ft4"> Para utilização do site, o CANDIDATO concorda integralmente em cumprir com todos os termos e condições abaixo, e outorga o seu</span>
                                <span className="ft5">CONSENTIMENTO EXPRESSO</span> para o tratamento de seus dados pessoais pelo O Amarelinho® com a finalidade específica de possibilitar o seu acesso, cadastro, e utilização da plataforma digital do O Amarelinho®, sendo que o seu consentimento, para os fins dispostos no art. 8º, da Lei nº 13.709 de 2018 com as alterações realizadas pela Lei  nº 13.853, de 08 de julho de 2019 ocorrerá de forma expressa:
                            </p>
                            <li className="p5 ft6">Clicando na caixa de aceitação dos termos exibida como rota de acesso à página exclusiva de serviços.</li>
                            <p className="p6 ft2">
                                <span className="ft1">1.2.</span>
                                <span className="ft4">O CANDIDATO pode, a qualquer tempo, revogar o seu consentimento ao tratamento de seus dados pessoais pelo O Amarelinho®, em requerimento que deverá ser dirigido ao endereço </span>
                                <a href="mailto:faleconosco@oamarelinho.com.br">
                                    <span className="ft7">faleconosco@oamarelinho.com.br</span>
                                </a>para o término deste tratamento e a eliminação dos dados pessoais do CANDIDATO, ressalvada a sua conservação para as finalidades dispostas no art. 16, e incisos, da Lei 13.709 de 2018, quais sejam: eventual comprovação de cumprimento de obrigação legal ou regulatória pelo controlador; estudo por órgão de pesquisa, observada a anonimização dos dados; transferência a terceiro, respeitados os requisitos legais de tratamento de dados; uso exclusivo do controlador.
                            </p>
                        </div>
                        <div id="page_2">
                            <p className="p7 ft9">
                                <span className="ft1">1.2.1.</span>
                                <span className="ft8">Caso o CANDIDATO manifeste a revogação do seu consentimento na forma do disposto acima, o presente Contrato será rescindido e não será devida qualquer devolução proporcional de valores já pagos ao O Amarelinho® pela compra do pacote.</span>
                            </p>
                            <p className="p8 ft2">
                                <span className="ft1">1.3.</span>
                                <span className="ft10">O CANDIDATO tem ciência de que as informações disponibilizadas no site poderão ser compartilhadas com as empresas parceiras do O Amarelinho® para garantir o acesso as candidaturas às vagas disponibilizadas, motivo pelo qual o CANDIDATO também concede o seu  </span>
                                <span className="ft5">CONSENTIMENTO EXPRESSO </span>para este compartilhamento, tendo em vista que este tratamento é condição para o fornecimento dos serviços contratados pelo CANDIDATO, conforme os arts. 7º, §5º, e art. 9º, §3º, da Lei 13.709 de 2018., com as alterações realizadas pela Lei nº 13.853, de 08 de julho de 2019
                            </p>
                            {/* <p className="p9 ft2">
                                <span className="ft1">1.4.</span>
                                <span className="ft11">Com o término de tratamento dos dados pessoais, o O Amarelinho® conservará os dados necessários às seguintes finalidades: eventual comprovação de cumprimento de obrigação legal ou regulatória pelo controlador; estudo por órgão de pesquisa, observada a anonimização dos dados; transferência a terceiro, respeitados os requisitos legais de tratamento de dados; uso exclusivo do controlador, vedado seu acesso a terceiro, e desde que anonimizados os dados.</span>
                            </p> */}
                            <p className="p10 ft0">2. REQUISITO PARA UTILIZAÇÃO</p>
                            <p className="p2 ft2">
                                <span className="ft1">2.1.</span>
                                <span className="ft4">Para utilização do site, o CANDIDATO deve ter idade mínima legal para oferecer serviços como profissional, ou seja, maior de 16 anos para a qualidade de Empregado e com no mínimo 14 anos para a condição de Jovem Aprendiz, devendo estar em pleno gozo de sua capacidade para praticar atos da vida civil.</span>
                            </p>
                            <p className="p1 ft0">3. OBJETO - USO DO SITE:</p>
                            <p className="p2 ft2">
                                <span className="ft1">3.1.</span>
                                <span className="ft10">O presente Termo tem por objeto a disponibilização de uma plataforma que permite a junção da procura por emprego pelo CANDIDATO com a oferta de emprego pelas empresas ANUNCIANTES em um ambiente online. Dessa forma, os CANDIDATOS registrados, ao se cadastrarem poderão incluir, modificar e enviar seu currículo, além de ter acesso às vagas de emprego/oportunidades de trabalho inicialmente divulgadas pelas empresas na versão impressa do </span>
                                <span className="ft3">O Amarelinho®.</span>
                                <span className="ft1">3.2.</span>
                                <span className="ft10">Pelo presente Termo, passa o CANDIDATO cadastrado a ter direito de utilizar seu acesso ao site para: (i) preencher o seu currículo; (ii) realizar o download do seu currículo; (iii) obter informações da vaga, com a exceção dos dados disponibilizados pelo Empregador que digam respeito ao endereço de comparecimento para o processo seletivo, data, horário, telefone e/ou e-mail de contato, e sem a opção de disparo de currículo para o e-mail do Empregador.</span>
                            </p>
                        </div>
                        <div id="page_3">
                            <p className="p11 ft13">
                                <span className="ft1">3.2.</span>
                                <span className="ft12">Pelo presente Termo, passa o CANDIDATO cadastrado a ter direito de utilizar seu acesso ao site para: (i) preencher o seu currículo; (ii) realizar o download do seu currículo; (iii) obter informações da vaga, com a exceção dos dados disponibilizados pelo Empregador que digam respeito ao endereço de comparecimento para o processo seletivo, data, horário, telefone e/ou </span>
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>&nbsp;
                                de contato, e sem a opção de disparo de currículo para o
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>&nbsp;
                                do Empregador.
                            </p>
                            <p className="p12 ft13">
                                <span className="ft1">3.2.1.</span>
                                <span className="ft14">Caso o CANDIDATO cadastrado adquira algum dos pacotes ofertados pelo O Amarelinho®, além dos direitos acima elencados, o CANDIDATO poderá: (i) obter informações detalhadas a respeito das vagas de emprego da semana e os dados de contato disponibilizados pelo Empregador ao anunciar a vaga, tais como endereço de comparecimento para o processo seletivo, data, horário, telefone e/ou </span>
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>
                                de contato; (ii) enviar o currículo cadastrado pela plataforma direto para o
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>
                                do Empregador, caso tenha sido por ele fornecido; (v) acesso ao Organizador Amarelinho para organização das entrevistas de emprego de seu interesse, observado que estas datas, acaso existentes, já serão divulgadas pelo Empregador no anúncio, de modo que não há direito à agendamento pelo
                            </p>
                            <p className="p13 ft1">CANDIDATO.</p>
                            <p className="p14 ft13">
                                <span className="ft1">3.2.1.1.</span>
                                <span className="ft12">NNa modalidade paga, os pacotes adquiridos pelos CANDIDATOS: (i) terão seus valores divulgados na plataforma do O Amarelinho®; (ii) poderão ter o seu valor corrigido ou oferecido de forma promocional; (iii) não são transferíveis a qualquer pessoa; (iv) serão cobrados por ciclo de publicação, a contar de Domingo a Domingo, devendo ser considerado Domingo como primeiro dia do ciclo e Sábado o último dia; (v) ficarão sujeitos à promoções sazonais e terão seu prazo de validade indicado na devida comunicação aos Usuários, seja por contato direto ou na própria página do Portal; (vi) poderão ter as formas de pagamentos alteradas a qualquer momento sem prejuízo do meio inicialmente escolhido pelo usuário; e (vii) poderão ter a cobrança de taxas bancárias extras a partir da escolha de uma determinada forma de pagamento.</span>
                            </p>
                            <p className="p15 ft13">
                                <span className="ft1">3.2.2.</span>
                                <span className="ft14">O Amarelinho® poderá alterar, a qualquer tempo, as condições previstas no tocante aos Planos, Produtos, Tarifas e Faturamento acima destacados, visando seu aprimoramento e melhoria dos serviços prestados, sendo que as novas condições entrarão em vigor imediatamente após publicação no site. Caso esta alteração modifique os presentes Termos de Uso, o CANDIDATO será informado dessa alteração, e será instado novamente a outorgar o seu consentimento expresso em relação às eventuais novas políticas adotadas.</span>
                            </p>
                        </div>
                        <div id="page_4">
                            <p className="p16 ft2">
                                <span className="ft1">3.1.3.</span>
                                <span className="ft4">Para os usuários cadastrados previamente às mudanças implementadas sobre formato de planos, não haverá cobrança automática para o plano pago. Estes à época de atualização do site, permanecerão no plano gratuito, porém nos mesmos moldes aplicados aos novos usuários do mesmo plano.</span>
                            </p>
                            <p className="p17 ft9">
                                <span className="ft1">3.1.3.1.</span>
                                <span className="ft15">O Amarelinho® não garante, de nenhuma forma, a colocação ou recolocação profissional do CANDIDATO, ou a existência de nenhuma vaga a qualquer Usuário, uma vez que isto dependerá de fatores externos a vontade deste (condições de mercado, economia, etc.).</span>
                            </p>
                            <p className="p18 ft9">
                                <span className="ft1">3.2.</span>
                                <span className="ft16">O Amarelinho® declara que manterá, segundo seu julgamento e possibilidades de mercado, a atualização constante do site visando seu aperfeiçoamento e adaptação às novas tecnologias disponíveis, bem como assegura ao usuário o correto tratamento dos dados fornecidos nos termos da lei 1379 de 2018 com as alterações realizadas pela Lei  nº 13.853, de 08 de julho de 2019</span>
                            </p>
                            <p className="p19 ft9">
                                <span className="ft1">3.3.</span>
                                <span className="ft17">O CANDIDATO, por seu lado, reconhece que a área de tecnologia da informação está sempre em constante desenvolvimento, e que a perfeição e ausência de defeitos ou riscos é um conceito inexistente nesse setor.</span>
                            </p>
                            <p className="p20 ft13">
                                <span className="ft1">3.4.</span>
                                <span className="ft18">O Amarelinho® manterá os dados cadastrais e de acesso do CANDIDATO confidenciais nos termos da lei 1379 de 2018 com as alterações realizadas pela Lei  nº 13.853, de 08 de julho de 2019, sendo que as informações apostas pelo CANDIDATO no seu currículo serão divulgadas no site apenas para as empresas cadastradas e com a finalidade específica de aproximar a busca do CANDIDATO (emprego) à busca do Empregador (empregado).</span>
                            </p>
                            <p className="p17 ft9">
                                <span className="ft1">3.5.</span>
                                <span className="ft17">O Amarelinho® se reserva o direito de recusar qualquer solicitação de cadastro e de cancelar um cadastro previamente aceito, desde que em desacordo com a legislação vigente ou as políticas e regras do presente Termo.</span>
                            </p>
                            <p className="p19 ft9">
                                <span className="ft1">3.6.</span>
                                <span className="ft19">O CANDIDATO reconhece que o cadastro de seu currículo em processos de seleção publicados no site implica na revelação de todas as informações apostas no currículo pelo próprio candidato para o anunciante da respectiva vaga e, por isso, assegura a possibilidade de remoção, ou alteração dos dados ofertados, conforme §5º do artigo 8º da lei 1379 de 2018 com as alterações realizadas pela Lei  nº 13.853, de 08 de julho de 2019</span>
                            </p>
                        </div>
                        <div id="page_5">
                            <p className="p7 ft9">
                                <span className="ft1">3.7.</span>
                                <span className="ft19">O CANDIDATO tem a opção de ocultar a visualização de seu currículo por parte dos empregadores, não recebendo nenhum e-mail ou informação sobre processos seletivos, exceto pelas empresas anunciantes na qual já tenha disponibilizado seu currículo, estando ciente que a ocultação não implica na remoção dos dados, o que poderá ser feito pelos meios próprios, conforme item 1.2. </span>
                            </p>
                            <p className="p21 ft6">
                                <span className="ft1">3.8.</span>
                                <span className="ft20">O CANDIDATO poderá reativar o seu cadastro a qualquer momento, desde que opte por isso ao efetuar seu login no site.</span>
                            </p>
                            <p className="p21 ft6">
                                <span className="ft1">3.9.</span>
                                <span className="ft21">O Amarelinho® prestará suporte técnico ao CANDIDATO para utilização plena do site, segundo as regras divulgadas no site.</span>
                            </p>
                            <p className="p22 ft9">
                                <span className="ft1">3.10.</span>
                                <span className="ft22">É de exclusiva responsabilidade e ônus do CANDIDATO providenciar os equipamentos de informática e a conexão à Internet necessários para o acesso ao site, não se responsabilizando O Amarelinho® em nenhuma hipótese pela existência, funcionamento e qualidade de tais equipamentos, sistemas e conexões.</span>
                            </p>
                            <p className="p23 ft13">
                                <span className="ft1">3.11.</span>
                                <span className="ft23">O CANDIDATO </span>
                                <span style={{ whiteSpace: 'nowrap' }}>compromete-se</span>&nbsp;
                                a acessar e utilizar o site exclusivamente para fins lícitos, segundo o sistema jurídico vigente, observando a melhor ética no uso de internet e a mais estrita
                                <span style={{ whiteSpace: 'nowrap' }}>boa-fé</span>&nbsp;
                                , bem como todas as regras de uso do site nele divulgadas, sendo o único responsável, pelas informações prestadas através de seu currículo e de seus dados cadastrais.
                            </p>
                            <p className="p3 ft9">
                                <span className="ft1">3.12.</span>
                                <span className="ft15">O CANDIDATO compreende e aceita que O Amarelinho® poderá suspender ou interromper o funcionamento do site para fins de manutenção ou por caso fortuito ou de força maior, independentemente de qualquer aviso prévio.</span>
                            </p>
                            <p className="p24 ft0">4. LEGISLAÇÃO APLICÁVEL</p>
                            <p className="p25 ft9">
                                <span className="ft1">4.1.</span>
                                <span className="ft17">O presente termo, assim como a utilização do site, será regido pelas leis vigentes na República Federativa do Brasil, observados também os usos e costumes que regem o comportamento ético na internet, e a mais estrita</span>
                                <span style={{ whiteSpace: 'nowrap' }}>boa-fé</span>.
                            </p>
                            <p className="p26 ft0">5. SENHAS E SEGURANÇA</p>
                            <p className="p27 ft6">
                                <span className="ft1">5.1.</span>
                                <span className="ft24">O CANDIDATO reconhece que sua senha de acesso ao site, é de uso pessoal e intransferível, não podendo</span>
                                <span style={{ whiteSpace: 'nowrap' }}>fornecê-la</span>&nbsp;
                                a terceiros sob nenhuma hipótese, devendo tomar
                            </p>
                        </div>
                        <div id="page_6">
                            <p className="p28 ft6">todas as providências cabíveis para garantir sua confidencialidade, devendo, inclusive, efetuar o logoff para finalizar seu acesso à sua página de serviços.</p>
                            <p className="p21 ft6">
                                <span className="ft1">5.2.</span>
                                <span className="ft21">O nível de segurança da senha, conforme combinação de letras, números e caracteres é de exclusiva responsabilidade do candidato.</span>
                            </p>
                            <p className="p21 ft6">
                                <span className="ft1">5.3.</span>
                                <span className="ft25">A recuperação de senhas perdidas ou esquecidas </span>
                                <span style={{ whiteSpace: 'nowrap' }}>dar-se-á</span>&nbsp;
                                conforme as normas de segurança divulgadas no site.
                            </p>
                            <p className="p29 ft9">
                                <span className="ft1">5.4.</span>
                                <span className="ft22">O CANDIDATO reconhece que deve providenciar segurança adequada no uso de seus equipamentos de informática, devendo utilizar, sempre que possível sistema de antivírus atualizados, firewalls e outros sistemas de segurança atualizados.</span>
                            </p>
                            <p className="p30 ft27">
                                <span className="ft1">5.5.</span>
                                <span className="ft26">O CANDIDATO </span>
                                <span style={{ whiteSpace: 'nowrap' }}>compromete-se</span>&nbsp;
                                a responder integralmente por quaisquer consequências jurídicas decorrentes diretamente da perda ou extravio de sua senha, devido a um comportamento seu considerado doloso ou culposo - imprudência, negligência, imperícia.
                            </p>
                            <p className="p9 ft9">
                                <span className="ft1">5.6.</span>
                                <span className="ft22">O CANDIDATO </span>
                                <span style={{ whiteSpace: 'nowrap' }}>compromete-se</span>
                                a notificar imediatamente o O Amarelinho® acerca de qualquer uso não autorizado de sua conta, quebra de segurança, bem como o qualquer tipo de acesso não autorizado por terceiros.
                            </p>
                            <p className="p9 ft9">
                                <span className="ft1">5.7.</span>
                                <span className="ft22">O Amarelinho® se obriga por meio de mecanismos físicos e tecnológicos, a segurança dos dados cadastrais dos Usuários e dos Signatários. Tais mecanismos atendem padrões razoáveis de cuidado, considerando-se as possibilidades técnicas e economicamente razoáveis da tecnologia aplicável à Internet em conformidade ao art. 48 da Lei nº 13.853, o Controlador comunicará ao Titular e à Autoridade Nacional de Proteção de Dados (ANPD) a ocorrência de incidente de segurança que possa acarretar risco ou dano relevante ao Titular.   </span>
                            </p>
                            <p className="p24 ft0">6. PROCESSOS SELETIVOS E VAGAS ANUNCIADAS</p>
                            <p className="p31 ft27">
                                <span className="ft1">6.1.</span>
                                <span className="ft26">O Amarelinho® atua como uma fornecedora de soluções que aproximam o CANDIDATO e as empresas anunciantes, e realiza a edição e a aprovação remota do conteúdo das vagas divulgadas pelas empresas ao CANDIDATO na versão impressa e digital.</span>
                            </p>
                            <p className="p22 ft9">
                                <span className="ft1">6.1.1.</span>
                                <span className="ft28">O Amarelinho® não tem a prerrogativa legal de realizar monitoramento e/ou fiscalização da veracidade das informações transmitidas pela empresa a respeito das características da vaga divulgada.</span>
                            </p>
                            <p className="p32 ft9">
                                <span className="ft1">6.2.</span>
                                <span className="ft15">O Amarelinho® não se responsabiliza pelo conteúdo ou a veracidade das vagas anunciadas, pelas comunicações estabelecidas entre as empresas e os CANDIDATOS, nem pela condução dos processos seletivos pelas empresas anunciantes, que são as únicas responsáveis pelas vagas, comunicações e seleções que anunciarem e/ou conduzirem, cabendo ao CANDIDATO zelar por seus melhores interesses, motivo pelo qual o CANDIDATO reconhece e concorda expressamente que o O Amarelinho® não poderá ser responsabilizado, perante o CANDIDATO ou qualquer terceiro, por perdas e danos de qualquer espécie que venham a ser conhecidas em decorrência de tais negociações, sejam contratuais, extracontratuais ou de qualquer outra natureza.</span>
                            </p>
                        </div>
                        <div id="page_7">
                            {/* <p className="p7 ft2">responsáveis pelas vagas, comunicações e seleções que anunciarem e/ou conduzirem, cabendo ao CANDIDATO zelar por seus melhores interesses, motivo pelo qual o CANDIDATO reconhece e concorda expressamente que o O Amarelinho® não poderá ser responsabilizado, perante o CANDIDATO ou qualquer terceiro, por perdas e danos de qualquer espécie que venham a ser conhecidas em decorrência de tais negociações, sejam contratuais, extracontratuais ou de qualquer outra natureza.</p> */}
                            <p className="p33 ft27">
                                <span className="ft1">6.3.</span>
                                <span className="ft29">O Amarelinho® </span>
                                <span style={{ whiteSpace: 'nowrap' }}>compromete-se</span>&nbsp;
                                a manter o currículo do CANDIDATO cadastrado conforme as funcionalidades do site, não se responsabilizando, entretanto, pela participação efetiva do CANDIDATO em processos seletivos (entrevistas), nem por sua contratação.
                            </p>
                            <p className="p30 ft27">
                                <span className="ft1">6.4.</span>
                                <span className="ft30">O CANDIDATO tem ciência de que, com sua anuência, a partir do seu cadastro e aceitação do Termo de Uso, poderão ser enviadas mensagens para seu
                                    <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>&nbsp;
                                    informando sobre vagas e processos seletivos abertos que estejam de acordo com o perfil cadastrado no site.
                                </span></p>
                            <p className="p34 ft0">7. RESPONSABILIDADES E PENALIDADES</p>
                            <p className="p35 ft27">
                                <span className="ft1">7.1.</span>
                                <span className="ft31">O Amarelinho® não se responsabiliza por vícios ou defeitos técnicos e/ou operacionais oriundos do sistema do CANDIDATO ou de terceiros, assim não se responsabiliza pela existência ou veracidade das informações das vagas anunciadas pelos recrutadores.</span>
                            </p>
                            <p className="p36 ft2">
                                <span className="ft1">7.2.</span>
                                <span className="ft11">O Amarelinho® é aceito pelo CANDIDATO na situação em que se encontra, e em hipótese alguma poderá ser cobrado por outras garantias e hipóteses, as quais incluem, mas não se limitam a: (i) pela adequação do site às necessidades ou expectativas do candidato; (ii) por resultados ou desempenhos esperados pelo candidato ao usar o site (iv) por um uso do site ininterrupto e livre de erros; (v) por qualquer informação ou comunicação veiculada pelas empresas anunciantes de vagas e processos seletivos; e (vi) pela correção e aperfeiçoamento dos erros encontrados pelo candidato no site.</span>
                            </p>
                            <p className="p37 ft2">
                                <span className="ft1">7.3.</span>
                                <span className="ft10">É expressamente vedado ao CANDIDATO o uso de expressões e termos pejorativos de ordem racial, sexual e/ou religiosa na concessão de informações e/ou fornecimento de seus dados, sendo estes os únicos e exclusivos responsáveis por tais informações e eventuais danos de qualquer natureza que esses possam causar, em caso de circulação dos mesmos na base de dados do O Amarelinho®.</span>
                            </p>
                        </div>
                        <div id="page_8">
                            <p className="p7 ft9">
                                <span className="ft1">7.4.</span>
                                <span className="ft22">É vedado ao CANDIDATO enviar juntamente com seu currículo, qualquer tipo ou forma de informação comercial, propaganda, correntes, anúncios caracterizados como “renda extra” ou qualquer conteúdo similar.</span>
                            </p>
                            <p className="p38 ft13">
                                <span className="ft1">7.5.</span>
                                <span className="ft12">Ambas as partes declaram ter pleno conhecimento de que o uso de qualquer sistema de informática em ambiente exposto à internet está sujeito a ataques de terceiros, não se responsabilizando nenhuma das partes perante a parte contrária, ou terceiros, por quaisquer danos causados por invasões no site por hackers, ou terceiros com intenções semelhantes.</span>
                            </p>
                            <p className="p37 ft9">
                                <span className="ft1">7.6.</span>
                                <span className="ft32">Sem prejuízo de outras medidas, O Amarelinho® poderá advertir, suspender ou cancelar, temporária ou definitivamente o cadastro de um Usuário ou aplicar uma sanção a este, a qualquer tempo, iniciando as ações legais cabíveis e/ou suspendendo a prestação de seus serviços se:</span>
                            </p>
                            <p className="p39 ft6">(a) o CANDIDATO não cumprir qualquer dispositivo destes Termos e Condições Gerais e demais políticas do O Amarelinho®.</p>
                            <p className="p40 ft1">(b) houver o descumprimento, total ou parcial, de seus deveres de CANDIDATO.</p>
                            <p className="p41 ft1">(c) se esse praticar atos fraudulentos ou dolosos;</p>
                            <p className="p39 ft6">(d) não puder ser verificada a identidade do CANDIDATO ou qualquer informação fornecida por ele esteja incorreta;</p>
                            <p className="p2 ft9">(e) O Amarelinho® entender que as informações ou qualquer atitude do CANDIDATO tenham causado algum dano a terceiros ou ao próprio O Amarelinho® ou tenham a potencialidade de assim o fazer.</p>
                            <p className="p2 ft9">
                                <span className="ft32">De modo geral, O Amarelinho® mantém seus dados pessoais pelo tempo necessário.
                                    Se o usuário não quiser mais que usemos suas informações, pode ser solicitado e seu acesso será fechado.</span>
                            </p>
                            <p className="p2 ft9">
                                <span className="ft32">Poderão ser retidas informações pessoais do usuário na medida necessária para cumprimento das obrigações legais do Amarelinho;</span>
                            </p>
                            <p className="p2 ft9">
                                <span className="ft32">Mesmo após o acesso de USUÁRIO ser removido algumas informações podem permanecer em nosso banco de dados, mas são desassociadas do seu USUÁRIO;</span>
                            </p>
                            <p className="p24 ft0">8. INTERRUPÇÃO</p>
                            <p className="p42 ft2">
                                <span className="ft1">8.1.</span>
                                <span className="ft33">Caso o O Amarelinho® tome ciência por si mesma, ou por qualquer terceiro, de que qualquer informação veiculada pelo CANDIDATO através do site esteja em desacordo com o sistema jurídico vigente, inclusive por conflito com direitos de terceiros, o CANDIDATO será comunicado pelo O Amarelinho® para que se manifeste sobre tais fatos no prazo máximo de 24 (vinte e quatro) horas.</span>
                            </p>
                            <p className="p43 ft35">
                                <span className="ft1">8.1.1.</span>
                                <span className="ft34">Caso a ilicitude fique constatada, o O Amarelinho® poderá excluir a informação, ou até mesmo desativar seu cadastro e currículo do site unilateralmente, independentemente de autorização do CANDIDATO, podendo fornecer todas as informações às autoridades legais, nos termos da lei.</span>
                            </p>
                        </div>
                        <div id="page_9">
                            {/* <p className="p28 ft6">autorização do CANDIDATO, podendo fornecer todas as informações às autoridades legais, nos termos da lei.</p> */}
                            <p className="p22 ft9">
                                <span className="ft1">8.1.2.</span>
                                <span className="ft22">Caso o CANDIDATO não se manifeste no prazo estabelecido acima, o O Amarelinho® poderá suspender tal conteúdo unilateralmente, independentemente de autorização do CANDIDATO, até sua devida regularização ou manifestação no sentido de demonstrar a adequação do conteúdo ao sistema jurídico vigente.</span>
                            </p>
                            <p className="p22 ft2">
                                <span className="ft1">8.1.3.</span>
                                <span className="ft33">Caso o O Amarelinho® constate que o conteúdo divulgado possa causar danos de qualquer espécie a terceiros ou envolva legislação de natureza penal, poderá</span>
                                <span style={{ whiteSpace: 'nowrap' }}>suspendê-lo</span>
                                , unilateralmente, antes de comunicar ao CANDIDATO para que se manifeste a respeito, nos termos do caput.
                            </p>
                            <p className="p9 ft9">
                                <span className="ft1">8.1.4.</span>
                                <span className="ft22">A suspensão ou exclusão de conteúdo também será unilateral e independentemente de qualquer comunicação ao CANDIDATO quando determinado por órgão do poder público competente.</span>
                            </p>
                            <p className="p44 ft37">
                                <span className="ft1">8.1.5.</span>
                                <span className="ft36">A suspensão de conteúdo ajustada nessa cláusula não dará direito à nenhuma espécie de indenização, bem como reparação de danos ou prejuízos pelo O Amarelinho® ao</span>
                            </p>
                            <p className="p40 ft1">CANDIDATO.</p>
                            <p className="p1 ft0">9. TOLERÂNCIA</p>
                            <p className="p45 ft13">
                                <span className="ft1">9.1.</span>
                                <span className="ft38">O não exercício, por qualquer das partes, de seus direitos ou prerrogativas legais ou contratuais, </span>
                                <span style={{ whiteSpace: 'nowrap' }}>constituir-se-á</span>
                                sempre em mera liberalidade, não constituindo precedente para futuros descumprimentos, nem alteração ou extinção das obrigações contratualmente assumidas. Também não caracterizam, em nenhuma hipótese, novação, transação, compensação ou remissão, nem se constitui em hipóteses de surrectio/supressio, podendo tais direitos ser exercidos a qualquer tempo, quando conveniente para o seu titular, inclusive para a exigência de obrigações vencidas e não cumpridas.
                            </p>
                            <p className="p46 ft0">
                                10.
                                <span style={{ whiteSpace: 'nowrap' }}>E-MAIL</span>
                                E ARMAZENAMENTO DE
                                <span style={{ whiteSpace: 'nowrap' }}>E-MAILS</span>
                            </p>
                            <p className="p47 ft6">
                                <span className="ft1">10.1.</span>
                                <span className="ft21">Por opção do CANDIDATO, este poderá autorizar expressamente o O Amarelinho®a enviar </span>
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>&nbsp;
                                pertinente a toda e qualquer comunicação proveniente de qualquer uma das
                            </p>
                        </div>
                        <div id="page_10">
                            <p className="p7 ft9">
                                empresas cadastradas no site ou proveniente do O Amarelinho®, bem como boletins periódicos ou informativos do site, mesmo que estes contenham links de outros sites e aplicativos, para o endereço de
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>&nbsp;
                                informado no cadastro do usuário.
                            </p>
                            <p className="p8 ft2">
                                <span className="ft1">10.2.</span>
                                <span className="ft11">O CANDIDATO reconhece o </span>
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>&nbsp;
                                cadastrado no ato da contratação como forma válida e eficaz de comunicação com o O Amarelinho®, sendo de sua responsabilidade a manutenção atualizada deste
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>&nbsp;
                                e de todos os seus dados cadastrais, sob pena da assunção das consequências eventualmente decorrentes do não recebimento de comunicações com o O Amarelinho® e de eventual inexatidão ou inautenticidade destes dados.
                            </p>
                            <section id="secPolitica" style={{ marginTop: "0px" }}></section>
                            <br />
                            <p className="p10 ft0">11. PRIVACIDADE E USO DE INFORMAÇÕES DO USUÁRIO</p>
                            <p className="p48 ft2">
                                <span className="ft1">11.1.</span>
                                <span className="ft39">O Amarelinho® respeita a privacidade de seus CANDIDATOS. E sendo assim, não monitorará, editará ou revelará nenhuma informação do CANDIDATO ou sobre o uso destes serviços pelo CANDIDATO, sem sua permissão, excetuando para o fim deste contrato e nas hipóteses de tal conduta ser necessária para: (a) proteger o interesse do O Amarelinho® ou de terceiros; (b) responder a eventual reclamação de que tal conteúdo viole direitos de terceiros; (c) identificar e resolver problemas técnicos; (d) cumprir procedimento legal, inclusive determinação judicial ou de qualquer órgão regulatório competente; (e) fazer cumprir os termos dos serviços ora prestados.</span>
                            </p>
                            <p className="p49 ft9">
                                <span className="ft1">11.2.</span>
                                <span className="ft22">Salvo por aceitação do CANDIDATO, o O Amarelinho® não armazenará informações pessoais disponíveis em sites de terceiros, tais como Twitter, Facebook, Linkedin e outros, para compartilhamento ou inclusão em suas bases de dados.</span>
                            </p>
                            <p className="p14 ft13">
                                <span className="ft1">11.3.</span>
                                <span className="ft12">O Amarelinho® informa que coleta informações anônimas de navegação dos usuários através de sistemas de terceiros autorizados, respeitadas todas as questões de segurança e privacidade de dados, com o objetivo de compreender melhor o perfil de nossa audiência, oferecer uma melhor experiência de navegação ou apresentar publicidade mais relevante para o usuário. Caso não queira que dados anônimos de navegação sejam utilizados, acesse e altere as configurações de privacidade do seu browser.</span>
                            </p>

                        </div>
                        <div id="page_11">
                            <p className="p7 ft2">
                                <span className="ft1">11.4.</span>
                                <span className="ft10">Considerando que o tratamento de dados pessoais do CANDIDATO é condição para o fornecimento dos serviços do O Amarelinho®, tendo em vista a necessidade de acesso aos dados para divulgação de vagas em cumprimento ao disposto pelos art. 9º, e 18 da Lei nº 13.709 de 2018 com as alterações realizadas pela Lei nº 13.853, de 08 de julho de 2019, O Amarelinho® informa que </span>
                            </p>
                            <p className="p39 ft6">(a) o controlador é a própria empresa e seus respectivos gestores;</p>
                            <p className="p39 ft6">(b) o contato para acesso a informação, remoção e solicitações dos usuários será feito por meio do
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail</span>&nbsp;
                                <a href="mailto:faleconosco@amarelinho.com.br"> faleconosco@amarelinho.com.br</a>;</p>
                            <p className="p39 ft6">(c) Os dados serão tratados somente pelo tempo necessário a prestação de serviços e poderão ser removidos a qualquer momento pelo usuário por meio do e-mail fornecido acima;</p>
                            <p className="p39 ft6">(d) os dados seguirão as regras de tratamento e compartilhamento devidamente descritas nas cláusulas 3.5 e 7 deste termo.</p>

                            <p className="p4 ft0">12. DIREITO DE PROPRIEDADE</p>
                            <p className="p27 ft9">
                                <span className="ft1">12.1.</span>
                                <span className="ft32">O CANDIDATO reconhece que todo o conteúdo (escrito, falado, imagem e som) exibido no sistema do O Amarelinho® ou estão protegidos pela Lei que regulamenta os direitos autorais, marcas, patentes e demais direitos de propriedade intelectual.</span>
                            </p>
                            <p className="p32 ft9">
                                <span className="ft1">12.2.</span>
                                <span className="ft19">O CANDIDATO </span>
                                <span style={{ whiteSpace: 'nowrap' }}>compromete-se</span>&nbsp;
                                a não utilizar tal conteúdo sem prévio e expresso conhecimento do O Amarelinho® ou empresas parceiras, sob pena de ser responsabilizado civil e criminalmente pelas violações que cometer aos direitos alheios.
                            </p>
                            <p className="p26 ft0">13. DISPOSIÇÕES GERAIS</p>
                            <p className="p2 ft2">
                                <span className="ft1">13.1.</span>
                                <span className="ft4">O CANDIDATO declara expressamente, por este instrumento, que, nos termos do Artigo 46 da Lei nº 8.078/1990 (Código de Defesa do Consumidor) e do art. 7º, I, da Lei 13.709 de 2018 (Lei de Proteção de Dados Pessoais), tomou conhecimento prévio deste Contrato no site do O Amarelinho®, tendo, inclusive, a possibilidade de
                                    <span style={{ whiteSpace: 'nowrap' }}>imprimi-lo</span>&nbsp;
                                    , e que avaliou, leu e concorda com todas as disposições e cláusulas aqui descritas.</span>
                            </p>
                            <p className="p32 ft2">
                                <span className="ft1">13.2.</span>
                                <span className="ft41">Fica ciente o CANDIDATO que quando ocorrer a alteração deste termo pelo O Amarelinho®, este tomara conhecimento imediatamente ao tentar logar no site, devendo para tanto ler e aceitar as condições do novo termo de uso antes de ser autorizado a acessar a página, sob pena de não conseguir entrar no site.</span>
                            </p>
                            <p className="p1 ft1">
                                <span className="ft1">13.3.</span>
                                <span className="ft42">O prazo de vigência dos presentes Termos de Uso e Política de Dados é indeterminado.</span>
                            </p>
                            <p className="p50 ft0">14. DO FORO</p>
                            <p className="p51 ft6">
                                <span className="ft1">14.1.</span>
                                <span className="ft43">As partes elegem o Foro da Comarca da Cidade de Barueri do Estado de São Paulo como competente para dirimir eventuais conflitos originários do presente contrato.</span>
                            </p>
                        </div>
                        <div id="page_12">
                            <p className="p7 ft5">
                                ESTE TERMO DE USO
                                <span style={{ whiteSpace: 'nowrap' }}>CONSIDERAR-SE-Á</span>&nbsp;
                                CELEBRADO E OBRIGATÓRIO ENTRE O O AMARELINHO® E O USUÁRIO NO MOMENTO EM QUE O USUÁRIO CONCLUIR O SEU CADASTRO E O PROCEDIMENTO PREVISTO NO O AMARELINHO®, SENDO CERTO QUE, ASSIM PROCEDENDO, O USUÁRIO DECLARA TER LIDO, COMPREENDIDO TODOS OS TERMOS E CONDIÇÕES DESTE TERMO DE USO, E OUTORGADO O SEU CONSENTIMENTO EXPRESSO PARA O TRATAMENTO DE SEUS DADOS PESSOAIS PELO O AMARELINHO®, RAZÃO PELA QUAL É RECOMENDÁVEL QUE O USUÁRIO IMPRIMA UMA CÓPIA DESTE DOCUMENTO PARA FUTURA REFERÊNCIA.
                            </p>
                            <p className="p49 ft9">
                                Em caso de dúvidas ou não aceitação de alguma disposição, não aceite este TERMO e entre em contato com o nosso serviço de atendimento pelo
                                <span style={{ whiteSpace: 'nowrap' }}>e-mail:</span>&nbsp;
                                <a href="mailto:faleconosco@oamarelinho.com.br">
                                    <span className="ft7">faleconosco@oamarelinho.com.br</span>
                                </a>
                            </p>
                            <p className="p52 ft0">Flamboyant Comunicações LTDA</p>
                            <p className="p53 ft45">Versão 2, de 07 de junho de 2021.</p>
                        </div>
                    </div>
                </div>
            </div>

        </>);
}
