import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function Genero(props: any) {

    const [name, setName] = useState('');
    const [documentNumber, setDocument] = useState('');
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        TagManager.dataLayer(dataLayerHelper.pagina('genero', 'cadastro-usuario'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setName(signup.name);
            setDocument(signup.document);
        } else {
            document.location = './';
        }
    }, [])

    function setGender(g: string) {
        SignupModelHelper.update({ gender: g })
        TagManager.dataLayer(dataLayerHelper.measureClick('genero', g));
        document.location = '#/nascimento-dia';
    }

    return (
        <>
            <Header step={4} total={17} title="Seu gênero" previous="#/cpf"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{documentNumber}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <p>
                                            <Typewriter
                                                onInit={(typewriter) => {
                                                    typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Por qual <strong>gênero deseja ser identificado?</strong>`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                        </p>
                                    </div>
                                </div>

                                { typed &&
                                <div className="gender-list">
                                    <div className="gender-item">
                                        <button className="btn btn-outline btn-full" onClick={() => setGender('Feminino')}>Feminino</button>
                                    </div>
                                    <div className="gender-item">
                                        <button className="btn btn-outline btn-full" onClick={() => setGender('Masculino')}>Masculino</button>
                                    </div>
                                    <div className="gender-item">
                                        <button className="btn btn-outline btn-full" onClick={() => setGender('Prefiro não informar')}>Prefiro não dizer</button>
                                    </div>
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
