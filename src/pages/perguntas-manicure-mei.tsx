import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function PerguntasManicureMei(props: any) {

    const [technique, setTechnique] = useState('');
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('mei', 'perguntas-especificas-manicure'));

        const signup = SignupModelHelper.getOngoingManicureQuestions();
        if (signup) {
            setTechnique(signup.technique);
        } else {
            document.location = './';
        }
    }, [])

    function setOption(mei: boolean) {
        SignupModelHelper.updateManicureQuestions({ mei });
        document.location = '#/perguntas-manicure-material';
    }

    let optionItemsList: any = [
        { label: "Sim, tenho MEI", value: true },
        { label: "Não tenho", value: false },
    ];

    let optionItems = optionItemsList.map((c: any) => {
        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className="btn btn-outline btn-full" onClick={() => setOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={2} total={3} title="Manicure" previous="#/perguntas-manicure-tecnica"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{technique}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`Possui <strong>MEI aberta?</strong>`)
                                                .callFunction(() => {
                                                    setTyped(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
