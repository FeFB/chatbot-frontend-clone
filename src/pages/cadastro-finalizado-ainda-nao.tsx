import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import { SignupModelHelper } from "../data/SignupModel";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function FinalizadoAindaNao(props: any) {

    const [fillCV, setFillCV] = useState<null | boolean>(null);
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        TagManager.dataLayer(dataLayerHelper.pagina('finalizado', 'cadastro-usuario'));
    }, [])

    useEffect(() => {
        if (fillCV === true) {
            TagManager.dataLayer(dataLayerHelper.pagina('finalizado-preencher', 'cadastro-usuario'));
            document.location = '#/objetivos-profissionais';
        }
        if (fillCV === false) {
            TagManager.dataLayer(dataLayerHelper.pagina('finalizado-nao-preencher', 'cadastro-usuario'));
            document.location = '/candidato/home.aspx';
        }
    }, [fillCV])


    return (
        <>
            <Header step={17} total={17} title="Cadastro finalizado" previous="#/termos" finished={true}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>Agora não</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <p>
                                            <Typewriter
                                                onInit={(typewriter) => {
                                                    typewriter.changeDelay(5).typeString(`
                                                    Sem problemas, mas <strong>não deixe de preencher seu currículo</strong>, 
                                                    você precisará dele para se candidatar às vagas de emprego.<br/><br/>
                                                    Até breve :)
                                                    `)
                                                        .callFunction(() => {
                                                            setTyped(true)
                                                        })
                                                        .start();
                                                }} />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-fixed">
                    <div className="container">
                        <div className="row d-flex align-items-center justify-content-center">
                            <div className="col-12 col-md-4">
                                <button className="btn btn-secondary btn-full">Fechar</button>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        </>);
}
