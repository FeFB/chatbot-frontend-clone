import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function PerguntasMotoristaVeiculo(props: any) {

    const [license, setLicense] = useState('');
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('veiculo', 'perguntas-especificas-motorista'));

        const signup = SignupModelHelper.get();
        const driverQuestions = SignupModelHelper.getOngoingDriverQuestions();

        if (signup) {
            setLicense(driverQuestions.license);
            const objectiveId = signup.objectives[ signup.objectives.length-1 ].id;
            if ( objectiveId === '2515' ) { // MOTOBOY
                setOption('moto');
            }
        } else {
            document.location = './#/perguntas-motorista-trabalhar-noite';
        }
    }, [])

    function setOption(vehicle: string) {
        SignupModelHelper.updateDriverQuestions({ vehicle, vehicleTrunk: null, vehicleCargo: null, vehicleMopp: null, vehicleYear: null })
        // TagManager.dataLayer(dataLayerHelper.measureClick('genero', g));
        var urlString = '';
        switch (vehicle) {
            case 'moto':
                urlString = 'bau';
                break;
            case 'carro':
                urlString = 'ano';
                break;
            case 'caminhao':
                urlString = 'carga';
                break;
            case 'nao-possuo':
                urlString = 'naopossuo';
                break;
        }
        document.location = '#/perguntas-motorista-veiculo-'+urlString;
    }

    let optionItemsList: any = [
        { label: "Moto", value: 'moto' },
        { label: "Carro", value: 'carro' },
        { label: "Caminhão", value: 'caminhao' },
        { label: "Não possuo", value: 'nao-possuo' }
    ];

    let optionItems = optionItemsList.map((c: any) => {
        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className="btn btn-outline btn-full" onClick={() => setOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={4} total={7} title="Motoristas ou Motoboys" previous="#/perguntas-motorista-habilitacao"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{license}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`E você <strong>possui veículo próprio?</strong> Escolha o principal relacionado ao cargo. Depois você poderá adicionar outros.`)
                                                .callFunction(() => {
                                                    setTyped(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
