import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import { dataLayerHelper } from "../data/dataLayerModel";
import TagManager from "react-gtm-module";
import { SignupModelHelper } from "../data/SignupModel";

export default function ExperienciaPossuiNao(props: any) {

    const [typed, setTyped] = useState(false);
    const [typed2, setTyped2] = useState(false);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('possui-nao', 'experiencia'));
    }, [] )

    return (
        <>
            <Header step={11} total={10} title="Sua experiência" previous="#/experiencias-profissionais" finished={false}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>Ainda não</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(5).typeString(`Não tem problema, <strong>algumas empresas contratam mesmo sem experiência</strong>.<br/> Mas nesse caso é importante fazer cursos na área para deixar seu currículo mais atrativo.`)
                                                    .pauseFor(1000)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {typed &&
                                    <div className="chat-item">
                                        <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                        <div className="chat-item-content">
                                            <Typewriter
                                                onInit={(typewriter) => {
                                                    typewriter.changeDelay(5).typeString(`E se <strong>já fez algum trabalho voluntário</strong>, também pode colocar nas experiências, as empresas valorizam bastante isso ;)`)
                                                        .pauseFor(1000)
                                                        .callFunction(() => {
                                                            setTyped2(true)
                                                        })
                                                        .start();
                                                }} />
                                        </div>
                                    </div>
                                }

                            </div>
                        </div>
                        {typed2 &&
                            <footer className="footer-fixed">
                                <div className="container">
                                    <div className="row d-flex align-items-center justify-content-center">
                                        <div className="col-12 col-md-4">
                                            <button className="btn btn-secondary btn-full" onClick={() => document.location = '#/cursos-inicio'}>Continuar</button>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        }

                    </div>
                </div>
            </div>
        </>);
}
