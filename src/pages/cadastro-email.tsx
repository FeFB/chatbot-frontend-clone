import { useState, useEffect } from "react";
import Header from "../components/header";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import ValidationService from "../services/PEXServices";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function Email(props: any) {

    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [typed, setTyped] = useState(false);
    const [typeEmail, setTypeEmail] = useState(false);
    const [interactions, setInteractions] = useState<InteractionModel[]>([]);

    useEffect(() => {
        setTyped(false);
        TagManager.dataLayer(dataLayerHelper.pagina('email', 'cadastro-usuario'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setPhone(signup.phone);
        } else {
            document.location = './';
        }
    }, [])

    useEffect(() => {
        if (typed)
            document.getElementById('footer-input')?.focus();
    }, [typed]);

    function enableEmail() {
        
        TagManager.dataLayer(dataLayerHelper.measureClick('email', 'digitar_email'));
        setTypeEmail(true);

        const chat = [...interactions, {
            text: 'Digitar e-mail',
            author: InteractionAuthor.CLIENTE
        }];
        setInteractions(chat);

        setTimeout(() => {
            document.getElementById('fld-email')?.focus();
        }, 1000)
    }

    async function checkEmail() {
        setTyped(false);
        const validateEmail = (emailCandidate: string) => {
            return String(emailCandidate)
                .toLowerCase()
                .match(
                    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                );
        };

        if (!validateEmail(email)) {

            const chat = [...interactions, {
                text: email,
                author: InteractionAuthor.CLIENTE
            }];
            setInteractions(chat);

            setTimeout(() => {
                setInteractions([...chat, {
                    author: InteractionAuthor.AMARELINHO,
                    text: 'Este e-mail não é válido.  Pode tentar novamente por favor?'
                }]);
                setTimeout(() => {
                    let chat = document.getElementsByClassName('chat')[0];
                    chat.scrollTo(0, chat.scrollHeight);
                }, 500);
            }, 1000)

        } else {

            const ret = await ValidationService.validateEmail(email.trim());

            if (!ret.success) {
                const chat = [...interactions, {
                    text: email,
                    author: InteractionAuthor.CLIENTE
                }];
                setInteractions(chat);
    
                setTimeout(() => {
                    if (ret.message?.indexOf('já está sendo utilizado') !== -1) {
                        ret.message += `&nbsp; Se você já possui cadastro no Amarelinho, tente fazer o <a href="/login.aspx">Login</a>.`;
                    }

                    setInteractions([...chat, {
                        author: InteractionAuthor.AMARELINHO,
                        text: ret.message ? ret.message : ''
                    }]);

                    setTimeout(() => {
                        let chat = document.getElementsByClassName('chat')[0];
                        chat.scrollTo(0, chat.scrollHeight);
                    }, 500);
                }, 1000)

            } else {

                SignupModelHelper.update({ email });
                document.location = '#/senha';

            }
        }
    }

    function noEmail() {
        SignupModelHelper.update({ email: 'Não tenho e-mail' });
        TagManager.dataLayer(dataLayerHelper.measureClick('email', 'nao_tenho_email'));
        document.location = '#/senha';
    }

    return (
        <>
            <Header step={14} total={17} title="Seu e-mail" previous="#/celular"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{phone}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Já está quase acabando!<br />
                                                        Preciso do seu <strong>e-mail de contato</strong>.<br />
                                                        Ele também será utilizado para falar com você!`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                        setTypeEmail(true)
                                                    }).start();
                                            }} />
                                    </div>
                                </div>

                                {/* { typed &&
                                <div className="email-list pb-5">
                                    <div className="gender-item">
                                        <button className="btn btn-outline btn-full" onClick={() => enableEmail()}>digitar e-mail</button>
                                    </div>
                                    <div className="gender-item">
                                        <button className="btn btn-outline btn-full" onClick={() => noEmail()}>Não tenho e-mail</button>
                                    </div>
                                </div>
                                } */}

                                {
                                    interactions.map((interaction) => {
                                        let objRet;
                                        
                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                            .callFunction(() => {
                                                                setTyped(true)
                                                            })
                                                            .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
                { typeEmail &&
                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="footer-answer-holder d-flex align-items-center justify-content-between">
                                    <input type="email" name="" id="footer-input" placeholder="Digite seu email" onChange={(e) => setEmail(e.target.value)} onKeyUp={(ev) => { if (ev.key === 'Enter') checkEmail() }} />
                                    {
                                        email.length === 0 &&
                                        <button className="btn-submit" disabled={true}>
                                            <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                        </button>
                                    }
                                    {
                                        email.length > 0 &&
                                        <button className="btn-submit" onClick={() => checkEmail()}>
                                            <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                        </button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                }
            </div>
        </>);
}




