import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function PerguntasMotoristaTrabalharFinalDeSemana(props: any) {

    const [workAtNight, setWorkAtNight] = useState(false);
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('trabalhar-finaldesemana', 'perguntas-especificas-motorista'));

        const signup = SignupModelHelper.getOngoingDriverQuestions();
        if (signup) {
            setWorkAtNight(signup.workAtNight);
        } else {
            document.location = './#/perguntas-motorista-trabalhar-noite';
        }
    }, [])

    function setOption(workWeekend: boolean) {
        SignupModelHelper.updateDriverQuestions({ workWeekend })
        document.location = '#/perguntas-motorista-habilitacao';
    }

    let optionItemsList: any = [
        { label: "Posso trabalhar", value: true },
        { label: "Não tenho disponibilidade", value: false },
    ];

    let optionItems = optionItemsList.map((c: any) => {
        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className="btn btn-outline btn-full" onClick={() => setOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={2} total={7} title="Motoristas ou Motoboys" previous="#/perguntas-motorista-trabalhar-noite"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{workAtNight ? 'Sim, tenho' : 'Não trabalho à noite'}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`E para <strong>trabalhar aos finais de semana?</strong>`)
                                                .callFunction(() => {
                                                    setTyped(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
