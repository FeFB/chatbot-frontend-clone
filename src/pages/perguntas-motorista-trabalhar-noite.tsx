import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function PerguntasMotoristaTrabalharNoite(props: any) {

    const [name, setName] = useState('');
    const [step, setStep] = useState(false);
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('trabalhar-noite', 'perguntas-especificas-motorista'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setName(signup.name);
        } else {
            document.location = './';
        }
    }, [])

    function setOption(workAtNight: boolean) {
        SignupModelHelper.updateDriverQuestions({ workAtNight });
        document.location = '#/perguntas-motorista-trabalhar-finaldesemana';
    }

    let optionItemsList: any = [
        { label: "Sim, tenho", value: true },
        { label: "Não trabalho à noite", value: false },
    ];

    let optionItems = optionItemsList.map((c: any) => {
        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className="btn btn-outline btn-full" onClick={() => setOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={1} total={7} title="Motoristas ou Motoboys" previous="#/objetivos-profissionais"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user d-none">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{name}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <p>
                                            <Typewriter
                                                onInit={(typewriter) => {
                                                    typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Vamos lá. Agora eu preciso fazer algumas perguntas sobre o seu cargo de interesse.`)
                                                    .callFunction(() => {
                                                        setStep(true)
                                                    })
                                                    .start();
                                            }} />
                                        </p>
                                    </div>
                                </div>
                                
                                { step &&
                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <p>
                                            <Typewriter
                                                onInit={(typewriter) => {
                                                    typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Você tem <strong>disponibilidade para trabalhar à noite?</strong>`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                        </p>
                                    </div>
                                </div>
                                }

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
