import { useState, useEffect } from "react";
import Header from "../components/header";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function NascimentoAno(props: any) {

    const [interactions, setInteractions] = useState<InteractionModel[]>([]);

    const [birthMonthName, setBirthMonthName] = useState('');
    const [birthMonthNumber, setBirthMonthNumber] = useState(0);

    const [birthDayName, setBirthDayName] = useState('');
    const [birthDayNumber, setBirthDayNumber] = useState(0);

    useEffect(() => {
        TagManager.dataLayer(dataLayerHelper.pagina('cep', 'cadastro-usuario'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setBirthMonthName(signup.birthMonth);
            setBirthMonthNumber(signup.birthMonthNumber);
            setBirthDayName(signup.birthDay);
            setBirthDayNumber(signup.birthDayNumber);
        } else {
            document.location = './';
        }
    }, [])

    function dateDiffInYears(dateold: Date, datenew: Date) {
        var ynew = datenew.getFullYear();
        var mnew = datenew.getMonth();
        var dnew = datenew.getDate();
        var yold = dateold.getFullYear();
        var mold = dateold.getMonth();
        var dold = dateold.getDate();
        var diff = ynew - yold;
        if (mold > mnew) diff--;
        else {
            if (mold == mnew) {
                if (dold > dnew) diff--;
            }
        }
        return diff;
    }

    function setBirthYear(year: number) {
        const maxYear = new Date().getFullYear() - 16;
        const dt = new Date(+year, birthMonthNumber - 1, birthDayNumber);
        if (isNaN(+year) || +year < 1900) {
            const chat = [...interactions, {
                text: year.toString(),
                author: InteractionAuthor.CLIENTE
            }];
            setInteractions(chat);

            setTimeout(() => {
                setInteractions([...chat, {
                    author: InteractionAuthor.AMARELINHO,
                    text: `Digite um ano entre 1900 e ${maxYear}`
                }]);
                setTimeout(() => {
                    let chat = document.getElementsByClassName('chat')[0];
                    chat.scrollTo(0, chat.scrollHeight);
                }, 500);
            }, 1000)
        } else {


            if (dt && dt.getDate() !== birthDayNumber) {
                const chat = [...interactions, {
                    text: year.toString(),
                    author: InteractionAuthor.CLIENTE
                }];
                setInteractions(chat);

                setTimeout(() => {
                    setInteractions([...chat, {
                        author: InteractionAuthor.AMARELINHO,
                        text: `A data ${birthDayNumber} de ${birthMonthName} de ${year} é inválida.`
                    }]);
                    setTimeout(() => {
                        let chat = document.getElementsByClassName('chat')[0];
                        chat.scrollTo(0, chat.scrollHeight);
                    }, 500);
                }, 1000)

            } else {
                if (dateDiffInYears(dt, new Date()) < 16) {
                    const chat = [...interactions, {
                        text: year.toString(),
                        author: InteractionAuthor.CLIENTE
                    }];
                    setInteractions(chat);

                    setTimeout(() => {
                        setInteractions([...chat, {
                            author: InteractionAuthor.AMARELINHO,
                            text: `É necessário ter 16(dezesseis) anos completos para prosseguir com o cadastro.`
                        }]);
                        setTimeout(() => {
                            let chat = document.getElementsByClassName('chat')[0];
                            chat.scrollTo(0, chat.scrollHeight);   
                        }, 500);
                    }, 1000)

                } else {
                    SignupModelHelper.update({
                        birthYear: year,
                        birthYearNumber: year,
                    });
                    document.location = '#/cep';
                }
            }
        }
    }

    const years = [];
    for (let x = new Date().getFullYear(); x > 1910; --x) {
        years.push(<option value={x}>{x}</option>);
    }

    return (
        <>
            <Header step={7} total={17} title="Seu nascimento - ano" previous="#/nascimento-mes"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{birthDayNumber} de {birthMonthName}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(15).typeString('E qual foi <strong>o ano do seu nascimento</strong>.')
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="footer-answer-holder d-flex align-items-center justify-content-between">
                                    <select name="" className="select select-year" id="year" onChange={e => setBirthYear(+e.target.value)}>
                                        <option>Selecione o ano</option>
                                        {years}
                                    </select>
                                    <input type="text" name="" id="input" placeholder="Escolha o ano" />
                                    <button>
                                        <svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.5556 2.4H18.3333V0H15.8889V2.4H6.11111V0H3.66667V2.4H2.44444C1.08778 2.4 0.0122222 3.48 0.0122222 4.8L0 21.6C0 22.92 1.08778 24 2.44444 24H19.5556C20.9 24 22 22.92 22 21.6V4.8C22 3.48 20.9 2.4 19.5556 2.4ZM19.5556 21.6H2.44444V9.6H19.5556V21.6ZM19.5556 7.2H2.44444V4.8H19.5556V7.2ZM7.33333 14.4H4.88889V12H7.33333V14.4ZM12.2222 14.4H9.77778V12H12.2222V14.4ZM17.1111 14.4H14.6667V12H17.1111V14.4ZM7.33333 19.2H4.88889V16.8H7.33333V19.2ZM12.2222 19.2H9.77778V16.8H12.2222V19.2ZM17.1111 19.2H14.6667V16.8H17.1111V19.2Z" fill="#6F5192" /></svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </>);
}
