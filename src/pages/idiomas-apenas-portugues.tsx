import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import { SignupModelHelper } from "../data/SignupModel";

export default function IdiomasApenasPortugues(props: any) {

    const [typed, setTyped] = useState(false);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('apenas-portugues', 'idiomas'));
    }, [] )

    return (
        <>
            <Header step={4} total={4} title="Idiomas" previous="#/idiomas-inicio" finished={false}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>Não, apenas o português</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(5).typeString(`Sem problemas!<br/>
                                                Mas, assim que houver possibilidade, pense em aprender um novo idioma. <br/>
                                                Isso abre muitas oportunidades de trabalho.`)
                                                    .pauseFor(1000)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                            </div>
                        </div>
                        {typed &&
                            <footer className="footer-fixed">
                                <div className="container">
                                    <div className="row d-flex align-items-center justify-content-center">
                                        <div className="col-12 col-md-4">
                                            <button className="btn btn-secondary btn-full" onClick={() => document.location = '#/carta-apresentacao'}>Continuar</button>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        }

                    </div>
                </div>
            </div>
        </>);
}
