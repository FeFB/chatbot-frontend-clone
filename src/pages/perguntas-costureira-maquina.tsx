import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function PerguntasCostureiraMaquina(props: any) {

    const [name, setName] = useState('');
    const [step, setStep] = useState(false);
    const [typed, setTyped] = useState(false);
    const [activeA, setActiveA] = useState(false);
    const [activeB, setActiveB] = useState(false);
    const [activeC, setActiveC] = useState(false);
    const [activeD, setActiveD] = useState(false);
    const [activeE, setActiveE] = useState(false);
    const [activeF, setActiveF] = useState(false);
    const [activeG, setActiveG] = useState(false);
    const [activeH, setActiveH] = useState(false);
    const [activeI, setActiveI] = useState(false);
    const [activeJ, setActiveJ] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('maquina', 'perguntas-especificas-costureira'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setName(signup.name);
        } else {
            document.location = './';
        }
    }, [])

    function activeOption(maquina: string) {

        if ( maquina!='J' )
            setActiveJ(false);

        switch (maquina) {
            case 'A':
                setActiveA(!activeA);
                break;
            case 'B':
                setActiveB(!activeB);
                break;
            case 'C':
                setActiveC(!activeC);
                break;
            case 'D':
                setActiveD(!activeD);
                break;
            case 'E':
                setActiveE(!activeE);
                break;
            case 'F':
                setActiveF(!activeF);
                break;
            case 'G':
                setActiveG(!activeG);
                break;
            case 'H':
                setActiveH(!activeH);
                break;
            case 'I':
                setActiveI(!activeI);
                break;

            case 'J':
                setActiveA(false);
                setActiveB(false);
                setActiveC(false);
                setActiveD(false);
                setActiveE(false);
                setActiveF(false);
                setActiveG(false);
                setActiveH(false);
                setActiveI(false);
                setActiveJ(!activeJ);
                break;
        }
    }

    function getMaquina():string {
        
        let r = '';
        if (activeA)
            r = 'Bordadeira';
        if (activeB)
            r += (r!=='' ? ', ' : '') + 'Casadeira';
        if (activeC)
            r += (r!=='' ? ', ' : '') + 'Fechadeira';
        if (activeD)
            r += (r!=='' ? ', ' : '') + 'Galoneira';
        if (activeE)
            r += (r!=='' ? ', ' : '') + 'Interloque';
        if (activeF)
            r += (r!=='' ? ', ' : '') + 'Overloque';
        if (activeG)
            r += (r!=='' ? ', ' : '') + 'Pespontadeira';
        if (activeH)
            r += (r!=='' ? ', ' : '') + 'Reta';
        if (activeI)
            r += (r!=='' ? ', ' : '') + 'Travete';
        if (activeJ)
            r += (r!=='' ? ', ' : '') + 'Nenhuma';

        return r;
    }

    function setOption(machine: string) {
        SignupModelHelper.updateDressmakerQuestions({ machine });
        document.location = '#/perguntas-costureira-tecido';
    }

    let optionItemsList: any = [
        { label: "Bordadeira", value: 'A' },
        { label: "Casadeira", value: 'B' },
        { label: "Fechadeira", value: 'C' },
        { label: "Galoneira", value: 'D' },
        { label: "Interloque", value: 'E' },
        { label: "Overloque", value: 'F' },
        { label: "Pespontadeira", value: 'G' },
        { label: "Reta", value: 'H' },
        { label: "Travete", value: 'I' },
        { label: "Nenhuma", value: 'J' },
    ];

    let optionItems = optionItemsList.map((c: any) => {

        let active = activeA;
        if (c.value === 'B')
            active = activeB;
        else if (c.value === 'C')
            active = activeC;
        else if (c.value === 'D')
            active = activeD;
        else if (c.value === 'E')
            active = activeE;
        else if (c.value === 'F')
            active = activeF;
        else if (c.value === 'G')
            active = activeG;
        else if (c.value === 'H')
            active = activeH;
        else if (c.value === 'I')
            active = activeI;
        else if (c.value === 'J')
            active = activeJ;

        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className={`btn btn-outline btn-full ${active ? 'active' : ''}`} onClick={() => activeOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={1} total={2} title="Costureira(o)" previous="#/objetivos-profissionais"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user d-none">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{name}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`Vamos lá. Agora eu preciso fazer algumas perguntas sobre o seu cargo de interesse.`)
                                                .callFunction(() => {
                                                    setStep(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>
                                
                                { step &&
                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`Com <strong>qual máquina você sabe trabalhar?</strong> <br>Pode escolher mais de uma:`)
                                                .callFunction(() => {
                                                    setTyped(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>
                                }

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-fixed">
                    <div className="container">
                        <div className="row d-flex align-items-center justify-content-center">
                            <div className="col-12 col-md-4">
                                <button className="btn btn-secondary btn-full" disabled={ getMaquina()==='' ? true : false } onClick={() => setOption( getMaquina() )}>Continuar</button>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

        </>);
}
