import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import SurveyServices from "../services/SurveyServices";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";

export default function PerguntasVendedorTipo(props: any) {

    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [name, setName] = useState('');
    const [step, setStep] = useState(false);
    const [typed, setTyped] = useState(false);
    const [activeA, setActiveA] = useState(false);
    const [activeB, setActiveB] = useState(false);
    const [activeC, setActiveC] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('tipo', 'perguntas-especificas-vendedor'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setName(signup.name);
        } else {
            document.location = './';
        }
    }, [])

    function getTecnica() {

        let r = '';
        if (activeA)
            r = 'Interna';
        if (activeB)
            r += (r!=='' ? ', ' : '') + 'Externa';
        if (activeC)
            r += (r!=='' ? ', ' : '') + 'Nenhuma';

        return r;
    }

    function activeOption(maquina: string) {

        if ( maquina!=='C' )
            setActiveC(false);

        switch (maquina) {
            case 'A':
                setActiveA(!activeA);
                break;
            case 'B':
                setActiveB(!activeB);
                break;

            case 'C':
                setActiveA(false);
                setActiveB(false);
                setActiveC(!activeC);
                break;
        }
    }

    function setOption(type: string) {
        SignupModelHelper.updateSellerQuestions({ type });

        const signup = SignupModelHelper.get();
        if (signup) {
            SurveyServices.saveSellerQuestion(signup).then((ret) => {
                if (!ret.success) {
                    setTimeout(() => {
                        setInteractions([...interactions, {
                            author: InteractionAuthor.AMARELINHO,
                            text: ret.message
                        }]);
                        setTimeout(() => {
                            let chat = document.getElementsByClassName('chat')[0];
                            chat.scrollTo(0, chat.scrollHeight);
                        }, 500);
                    }, 1000)
                } else {
                    SignupModelHelper.update({ sellerQuestions: null });
                    TagManager.dataLayer(dataLayerHelper.formEnviado('form_perguntas-especificas-vendedor'));
                    document.location = '#/experiencias-profissionais';
                }
            }).catch((ex) => {
                setTimeout(() => {
                    setInteractions([...interactions, {
                        author: InteractionAuthor.AMARELINHO,
                        text: ex
                    }]);
                    setTimeout(() => {
                        let chat = document.getElementsByClassName('chat')[0];
                        chat.scrollTo(0, chat.scrollHeight);
                    }, 500);
                }, 1000)
            });
        }      
    }

    let optionItemsList: any = [
        { label: "Interna", value: 'A' },
        { label: "Externa", value: 'B' },
        { label: "Nenhuma", value: 'C' },
    ];

    let optionItems = optionItemsList.map((c: any) => {
        let active = activeA;

        if (c.value === 'B')
            active = activeB;
        else if (c.value === 'C')
            active = activeC;

        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className={`btn btn-outline btn-full ${active ? 'active' : ''}`} onClick={() => activeOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={1} total={1} title="Vendedor(a)" previous="#/objetivos-profissionais"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user d-none">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{name}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`Vamos lá. Agora eu preciso fazer algumas perguntas sobre o seu cargo de interesse.`)
                                                .callFunction(() => {
                                                    setStep(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>
                                
                                { step &&
                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`Você <strong>tem experiência com qual tipo de venda?</strong>`)
                                                .callFunction(() => {
                                                    setTyped(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>
                                }

                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">                                                    
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <footer className={`footer-fixed ${!typed ? 'd-none' : ''}`}>
                    <div className="container">
                        <div className="row d-flex align-items-center justify-content-center">
                            <div className="col-12 col-md-4">
                                <button className="btn btn-secondary btn-full" disabled={ getTecnica()==='' ? true : false } onClick={() => setOption( getTecnica() )}>Continuar</button>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

        </>);
}
