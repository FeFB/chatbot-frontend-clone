import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import SurveyServices from "../services/SurveyServices";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";

export default function PerguntasCostureiraTecido(props: any) {

    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [machine, setMachine] = useState('');
    const [typed, setTyped] = useState(false);
    const [activeA, setActiveA] = useState(false);
    const [activeB, setActiveB] = useState(false);
    const [activeC, setActiveC] = useState(false);
    const [activeD, setActiveD] = useState(false);
    const [activeE, setActiveE] = useState(false);
    const [activeF, setActiveF] = useState(false);
    const [activeG, setActiveG] = useState(false);
    const [activeH, setActiveH] = useState(false);
    const [activeI, setActiveI] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('tecido', 'perguntas-especificas-costureira'));

        const signup = SignupModelHelper.getOngoingDressmakerQuestions();
        if (signup) {
            setMachine(signup.machine);
        } else {
            document.location = './';
        }
    }, []);

    function getTecido() {

        let r = '';
        if (activeA)
            r = 'Seda';
        if (activeB)
            r += (r!=='' ? ', ' : '') + 'Alfaiataria';
        if (activeC)
            r += (r!=='' ? ', ' : '') + 'Malha';
        if (activeD)
            r += (r!=='' ? ', ' : '') + 'Tectel';
        if (activeE)
            r += (r!=='' ? ', ' : '') + 'Nylon';
        if (activeF)
            r += (r!=='' ? ', ' : '') + 'Linho';
        if (activeG)
            r += (r!=='' ? ', ' : '') + 'Algodão';
        if (activeH)
            r += (r!=='' ? ', ' : '') + 'TNT';
        if (activeI)
            r += (r!=='' ? ', ' : '') + 'Nenhum';

        return r;
    }

    function activeOption(maquina: string) {

        if ( maquina!='I' )
            setActiveI(false);

        switch (maquina) {
            case 'A':
                setActiveA(!activeA);
                break;
            case 'B':
                setActiveB(!activeB);
                break;
            case 'C':
                setActiveC(!activeC);
                break;
            case 'D':
                setActiveD(!activeD);
                break;
            case 'E':
                setActiveE(!activeE);
                break;
            case 'F':
                setActiveF(!activeF);
                break;
            case 'G':
                setActiveG(!activeG);
                break;
            case 'H':
                setActiveH(!activeH);
                break;

            case 'I':
                setActiveA(false);
                setActiveB(false);
                setActiveC(false);
                setActiveD(false);
                setActiveE(false);
                setActiveF(false);
                setActiveG(false);
                setActiveH(false);
                setActiveI(!activeI);
                break;
        }
    }

    function setOption(cloth: string) {
        SignupModelHelper.updateDressmakerQuestions({ cloth });

        const signup = SignupModelHelper.get();
        if (signup) {
            SurveyServices.saveDressmakerQuestion(signup).then((ret) => {
                if (!ret.success) {
                    setTimeout(() => {
                        setInteractions([...interactions, {
                            author: InteractionAuthor.AMARELINHO,
                            text: ret.message
                        }]);
                        setTimeout(() => {
                            let chat = document.getElementsByClassName('chat')[0];
                            chat.scrollTo(0, chat.scrollHeight);
                        }, 500);
                    }, 1000)
                } else {
                    SignupModelHelper.update({ dressmakerQuestions: null });
                    TagManager.dataLayer(dataLayerHelper.formEnviado('form_perguntas-especificas-costureira'));
                    document.location = '#/experiencias-profissionais';
                }
            }).catch((ex) => {
                setTimeout(() => {
                    setInteractions([...interactions, {
                        author: InteractionAuthor.AMARELINHO,
                        text: ex
                    }]);
                    setTimeout(() => {
                        let chat = document.getElementsByClassName('chat')[0];
                        chat.scrollTo(0, chat.scrollHeight);
                    }, 500);
                }, 1000)
            });
        }
    }

    let optionItemsList: any = [
        { label: "Seda", value: 'A' },
        { label: "Alfaiataria", value: 'B' },
        { label: "Malha", value: 'C' },
        { label: "Tectel", value: 'D' },
        { label: "Nylon", value: 'E' },
        { label: "Linho", value: 'F' },
        { label: "Algodão", value: 'G' },
        { label: "TNT", value: 'H' },
        { label: "Nenhum", value: 'I' },
    ];

    let optionItems = optionItemsList.map((c: any) => {

        let active = activeA;
        if (c.value === 'B')
            active = activeB;
        else if (c.value === 'C')
            active = activeC;
        else if (c.value === 'D')
            active = activeD;
        else if (c.value === 'E')
            active = activeE;
        else if (c.value === 'F')
            active = activeF;
        else if (c.value === 'G')
            active = activeG;
        else if (c.value === 'H')
            active = activeH;
        else if (c.value === 'I')
            active = activeI;

        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className={`btn btn-outline btn-full ${active ? 'active' : ''}`} onClick={() => activeOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={2} total={2} title="Costureira(o)" previous="#/perguntas-costureira-maquina"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{machine}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`E com <strong>quais tecidos</strong> você possui experiência?`)
                                                .callFunction(() => {
                                                    setTyped(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">                                                    
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }                                

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-fixed">
                    <div className="container">
                        <div className="row d-flex align-items-center justify-content-center">
                            <div className="col-12 col-md-4">
                                <button className="btn btn-secondary btn-full" disabled={ getTecido()==='' ? true : false } onClick={() => setOption( getTecido() )}>Continuar</button>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

        </>);
}
