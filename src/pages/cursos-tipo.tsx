import { useEffect, useState } from "react";
import Header from "../components/header";
import { CourseType, SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function CursosTipo(props: any) {

    const [typed, setTyped] = useState(false);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('tipo', 'cursos'));
    }, [])

    function setCourseType(g: CourseType) {
        SignupModelHelper.updateCourse({ type: g })

        if (g === CourseType.OUTRO) {
            document.location = '#/cursos-outro';
        } else {
            SignupModelHelper.updateCourse({ typeOther: null })
            document.location = '#/cursos-curso';
        }
    }

    return (
        <>
            <Header step={2} total={10} title="Cursos" previous="#/cursos-inicio"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>Sim, já tenho.</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Muito bom!<br/>
                                                    Qual o <strong>tipo de curso</strong> você fez?`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {typed &&
                                    <div className="professional-list">
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setCourseType(CourseType.APRENDIZAGEM)}>Aprendizagem</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setCourseType(CourseType.CONCURSO)}>Concurso</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setCourseType(CourseType.CURSO_LIVRE)}>Curso livre</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setCourseType(CourseType.GRADUACAO_E_POS)}>Graduação e pós</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setCourseType(CourseType.IDIOMA)}>Idioma</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setCourseType(CourseType.TECNICO)}>Técnico</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setCourseType(CourseType.OUTRO)}>Outro</button>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>);
}
