import { useState, useEffect } from "react";
import Header from "../components/header";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import { AddressModel, SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function EnderecoLogradouro(props: any) {

    const [typed, setTyped] = useState(false);

    const [logradouro, setLogradouro] = useState('');
    const [cep, setCep] = useState('');
    const [objectAddress, setObjectAddress] = useState<AddressModel>();
    const [completeAddress, setCompleteAddress] = useState('');

    const [interactions, setInteractions] = useState<InteractionModel[]>([]);

    useEffect(() => {
        setTyped(false);
        TagManager.dataLayer(dataLayerHelper.pagina('end-logradouro', 'cadastro-usuario'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setCep(signup.address?.zipCode);
            setObjectAddress(signup.address);
            
            const address = `
                Cep: ${signup.address.zipCode}<br/><br/>
                Cidade: ${signup.address.city}<br/>
                Estado: ${signup.address.state}<br/>
            `;
            setCompleteAddress(address);

        } else {
            document.location = './';
        }
    }, [])

    useEffect(() => {
        if (typed)
            document.getElementById('footer-input')?.focus();
    }, [typed]);

    function checkAddLogradouro() {
        setTyped(false);
        if (logradouro.trim().length <= 0) {
            const chat = [...interactions, {
                text: logradouro,
                author: InteractionAuthor.CLIENTE
            }];
            setInteractions(chat);

            setTimeout(() => {
                setInteractions([...chat, {
                    author: InteractionAuthor.AMARELINHO,
                    text: 'Preencha corretamente com o seu endereço'
                }]);
                setTimeout(() => {
                    let chat = document.getElementsByClassName('chat')[0];
                    chat.scrollTo(0, chat.scrollHeight);
                }, 500);
            }, 1000)
        } else {

            const objAddress = {
                ...objectAddress,
                street: logradouro
            } as AddressModel;

            SignupModelHelper.update({ address: objAddress });
            document.location = '#/endereco-numero';
        }
    }

    return (
        <>
            <Header step={9} total={17} title="Seu endereço" previous="#/cep"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{cep}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        {
                                            completeAddress &&
                                            <Typewriter
                                                onInit={(typewriter) => {
                                                    typewriter
                                                        .changeDelay(15)
                                                        .typeString(`Olha só, conseguimos identificar os dados para o seu CEP:<br />${completeAddress}<br />
                                                        Agora precisamos do seu <strong>endereço</strong>, por favor.`)
                                                        .callFunction(() => {
                                                            setTyped(true)
                                                        })
                                                        .start();
                                                }} />
                                        }
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let ret = <></>;
                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            ret = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                                .callFunction(() => {
                                                                    setTyped(true)
                                                                })
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            ret = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }
                                        return ret;
                                    })
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="footer-answer-holder d-flex align-items-center justify-content-between">
                                    <input type="text" name="" id="footer-input" placeholder="Digite o seu endereço" onChange={(e) => setLogradouro(e.target.value)} onKeyUp={(ev) => { if (ev.key === 'Enter') checkAddLogradouro() }} />
                                    {
                                        logradouro.length === 0 &&
                                        <button className="btn-submit" disabled={true}>
                                            <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                        </button>
                                    }
                                    {
                                        logradouro.length > 0 &&
                                        <button className="btn-submit" onClick={checkAddLogradouro}>
                                            <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                        </button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        </>);
}




