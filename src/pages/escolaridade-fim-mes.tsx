import { useState, useEffect } from "react";
import Header from "../components/header";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import { GraduationModel, SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import PEXServices from "../services/PEXServices";

export default function EscolaridadeFimMes(props: any) {

    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [graduation, setGraduation] = useState<GraduationModel | null>(null);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('fim-mes', 'escolaridade'));
        setGraduation(SignupModelHelper.getOngoingGraduation());
    }, [])

    function setEntranceMonth(month: number) {
        let chat: any = [];
        if (isNaN(+month) || +month > 12) {
            chat = [...interactions, {
                text: month.toString(),
                author: InteractionAuthor.CLIENTE
            }];
            setInteractions(chat);

            setTimeout(() => {
                setInteractions([...chat, {
                    author: InteractionAuthor.AMARELINHO,
                    text: `Digite um mês entre Janeiro e Dezembro`
                }]);
                setTimeout(() => {
                    let chat = document.getElementsByClassName('chat')[0];
                    chat.scrollTo(0, chat.scrollHeight);
                }, 500);
            }, 1000)
        } else {            
            if (graduation?.finish) {
                SignupModelHelper.updateGraduation({ finish: new Date(new Date(graduation?.finish).getFullYear(), month - 1, 1) });
            }

            const signup = SignupModelHelper.updateGraduation({ saved: true });

            if (signup) {
                PEXServices.saveGraduation(signup).then((ret) => {
                    if (!ret.success) {
                        setTimeout(() => {
                            setInteractions([...chat, {
                                author: InteractionAuthor.AMARELINHO,
                                text: ret.message
                            }]);
                            setTimeout(() => {
                                let chat = document.getElementsByClassName('chat')[0];
                                chat.scrollTo(0, chat.scrollHeight);
                            }, 500);
                        }, 1000)
                    } else {
                        TagManager.dataLayer(dataLayerHelper.formEnviado('form_escolaridade'));
                        SignupModelHelper.update({ graduations: null });
                        document.location = '#/cursos-inicio';
                    }
                }).catch((ex) => {
                    setTimeout(() => {
                        setInteractions([...chat, {
                            author: InteractionAuthor.AMARELINHO,
                            text: ex
                        }]);
                        setTimeout(() => {
                            let chat = document.getElementsByClassName('chat')[0];
                            chat.scrollTo(0, chat.scrollHeight);
                        }, 500);
                    }, 1000)
                });
            }
        }
    }

    let arrMonths: string[] = ['', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    const months = [];
    for (let x = 1; x <= 12; ++x) {
        months.push(<option key={x} value={x}>{arrMonths[x]}</option>);
    }

    return (
        <>
            <Header step={8} total={8} title="Escolaridade" previous="#/escolaridade-fim-ano"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>
                                            {graduation && `${new Date(graduation?.finish).getFullYear()}`}
                                        </p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(15).typeString('E o <strong>mês de saída</strong>.')
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="footer-answer-holder d-flex align-items-center justify-content-between">
                                    <select name="" className="select select-year" id="year" onChange={e => setEntranceMonth(+e.target.value)}>
                                        <option>Selecione o mês</option>
                                        {months}
                                    </select>
                                    <input type="text" name="" id="input" placeholder="Escolha o mês" />
                                    <button>
                                        <svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.5556 2.4H18.3333V0H15.8889V2.4H6.11111V0H3.66667V2.4H2.44444C1.08778 2.4 0.0122222 3.48 0.0122222 4.8L0 21.6C0 22.92 1.08778 24 2.44444 24H19.5556C20.9 24 22 22.92 22 21.6V4.8C22 3.48 20.9 2.4 19.5556 2.4ZM19.5556 21.6H2.44444V9.6H19.5556V21.6ZM19.5556 7.2H2.44444V4.8H19.5556V7.2ZM7.33333 14.4H4.88889V12H7.33333V14.4ZM12.2222 14.4H9.77778V12H12.2222V14.4ZM17.1111 14.4H14.6667V12H17.1111V14.4ZM7.33333 19.2H4.88889V16.8H7.33333V19.2ZM12.2222 19.2H9.77778V16.8H12.2222V19.2ZM17.1111 19.2H14.6667V16.8H17.1111V19.2Z" fill="#6F5192" /></svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </>);
}
