import { useState, useEffect } from "react";
import Header from "../components/header";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import { AddressModel, SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function Celular(props: any) {

    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [objectAddress, setObjectAddress] = useState<AddressModel>();
    const [phone, setPhone] = useState('');
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        setTyped(false);
        TagManager.dataLayer(dataLayerHelper.pagina('celular', 'cadastro-usuario'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setObjectAddress(signup.address);
        } else {
            document.location = './';
        }
    }, []);

    useEffect(() => {
        if (typed)
            document.getElementById('footer-input')?.focus();
    }, [typed]);

    function setMobilePhone() {
        setTyped(false);
        const cleanPhone = phone.replaceAll(/[a-z ()\-.]/gi, '');

        if (cleanPhone.length !== 11) {
            const chat = [...interactions, {
                text: phone,
                author: InteractionAuthor.CLIENTE
            }];
            setInteractions(chat);

            setTimeout(() => {
                setInteractions([...chat, {
                    author: InteractionAuthor.AMARELINHO,
                    text: 'Seu telefone deve ter 11 números (2 do DDD e 9 do seu número).'
                }])
                setTimeout(() => {
                    setTyped(true);
                    let chat = document.getElementsByClassName('chat')[0];
                    chat.scrollTo(0, chat.scrollHeight);
                }, 500);
            }, 1000);

        } else if (isNaN(+cleanPhone)) {

            const chat = [...interactions, {
                text: phone,
                author: InteractionAuthor.CLIENTE
            }];
            setInteractions(chat);

            setTimeout(() => {
                setInteractions([...chat, {
                    author: InteractionAuthor.AMARELINHO,
                    text: 'Informe seu telefone usando somente números.'
                }])
                setTimeout(() => {
                    setTyped(true);
                    let chat = document.getElementsByClassName('chat')[0];
                    chat.scrollTo(0, chat.scrollHeight);
                }, 500);
            }, 1000);

        } else {
            SignupModelHelper.update({
                phone: cleanPhone,
            });
            document.location = '#/email';
        }
    }

    return (
        <>
            <Header step={13} total={17} title="Seu celular" previous={`#/endereco-${objectAddress?.typedStreet ? 'bairro' : 'complemento'}`}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{objectAddress?.typedStreet ? objectAddress?.neighborhood : objectAddress?.complement}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(15).typeString(`Informe também <strong>seu número de celular com o DDD</strong>.<br/>
                                                    E não esqueça de <strong>mantê-lo atualizado</strong> em nossa plataforma. Ele é muito importante para a gente se falar.`)
                                                    .callFunction(() => {
                                                        setTyped(true);
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>
                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                            .callFunction(() => {
                                                                setTyped(true)
                                                            })
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }
                                        return objRet;
                                    })
                                }

                            </div>
                        </div>
                    </div>
                </div>


                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="footer-answer-holder d-flex align-items-center justify-content-between">
                                    <input type="text" name="" id="footer-input" placeholder="Digite seu telefone com DDD" onChange={(e) => setPhone(e.target.value)} onKeyUp={(ev) => { if (ev.key === 'Enter') setMobilePhone() }} />
                                    {
                                        phone.length === 0 &&
                                        <button className="btn-submit" disabled={true}>
                                            <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                        </button>
                                    }
                                    {
                                        phone.length > 0 &&
                                        <button className="btn-submit" onClick={() => setMobilePhone()}>
                                            <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                        </button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

        </>);
}




