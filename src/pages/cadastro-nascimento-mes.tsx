import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import { dataLayerHelper } from "../data/dataLayerModel";
import TagManager from "react-gtm-module";

export default function NascimentoMes(props: any) {

    const [gender, setGender] = useState('');
    const [typed, setTyped] = useState(false);
    const [birthDayNumber, setBirthDayNumber] = useState(0);

    useEffect(() => {
        TagManager.dataLayer(dataLayerHelper.pagina('nasc-mes', 'cadastro-usuario'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setBirthDayNumber(signup.birthDayNumber);
        } else {
            document.location = './';
        }
    }, [])

    function setBirthMonth(monthNumber: number) {
        let monthName: string = '';
        let arrMonths: string[] = ['', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        SignupModelHelper.update({
            birthMonthNumber: monthNumber,
            birthMonth: arrMonths[monthNumber]
        });
        TagManager.dataLayer(dataLayerHelper.measureClick('data-nascimento', 'mes'));
        document.location = '#/nascimento-ano';
    }


    return (
        <>
            <Header step={6} total={17} title="Seu nascimento - mês" previous="#/nascimento-dia"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{birthDayNumber}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString('Agora me diga <strong>o mês</strong>.')
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="footer-answer-holder d-flex align-items-center justify-content-between">
                                    <select name="" className="select select-month" id="month" onChange={(e) => setBirthMonth(+e.target.value)}>
                                        <option>Selecione o mês</option>
                                        <option value="1">Janeiro</option>
                                        <option value="2">Fevereiro</option>
                                        <option value="3">Março</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Maio</option>
                                        <option value="6">Junho</option>
                                        <option value="7">Julho</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Setembro</option>
                                        <option value="10">Outubro</option>
                                        <option value="11">Novembro</option>
                                        <option value="12">Dezembro</option>
                                    </select>
                                    <input type="text" name="" id="input" placeholder="Escolha o mês" />
                                    <button>
                                        <svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.5556 2.4H18.3333V0H15.8889V2.4H6.11111V0H3.66667V2.4H2.44444C1.08778 2.4 0.0122222 3.48 0.0122222 4.8L0 21.6C0 22.92 1.08778 24 2.44444 24H19.5556C20.9 24 22 22.92 22 21.6V4.8C22 3.48 20.9 2.4 19.5556 2.4ZM19.5556 21.6H2.44444V9.6H19.5556V21.6ZM19.5556 7.2H2.44444V4.8H19.5556V7.2ZM7.33333 14.4H4.88889V12H7.33333V14.4ZM12.2222 14.4H9.77778V12H12.2222V14.4ZM17.1111 14.4H14.6667V12H17.1111V14.4ZM7.33333 19.2H4.88889V16.8H7.33333V19.2ZM12.2222 19.2H9.77778V16.8H12.2222V19.2ZM17.1111 19.2H14.6667V16.8H17.1111V19.2Z" fill="#6F5192" /></svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        </>);
}
