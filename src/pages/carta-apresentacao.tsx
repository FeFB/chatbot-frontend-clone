import Header from "../components/header";
import Typewriter from "typewriter-effect";
import { useEffect, useState } from "react";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import { SignupModelHelper } from "../data/SignupModel";
import PEXServices from "../services/PEXServices";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";

export default function CartaApresentacao(props: any) {

    const [typed, setTyped] = useState(false);
    const [letter, setLetter] = useState('');
    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const countLetter = 200;

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });

        document.getElementById('footer-input')?.addEventListener('keydown', (e) => {

            let div = e.target as HTMLDivElement;
            let len = div.innerText.length;
            let special = ['Backspace', 'ArrowLeft', 'ArrowRight', 'ArrowDown', 'ArrowUp'];
            
            let isSpecial = false;
            special.map( key => {
                if (key === e.key) {
                    isSpecial = true;
                }
                return false;
            });

            if ( isSpecial )
                return true;

            if (len >= countLetter) {
                e.preventDefault();
                return false;
            }
        });

        TagManager.dataLayer(dataLayerHelper.pagina('apresentacao', 'carta-apresentacao'));
    }, [])

    useEffect(() => {
        if (typed)
            document.getElementById('footer-input')?.focus();
    }, [typed]);

    function checkPresentationLetter() {
        
        async function savePresentationLetter() {

            const signup = SignupModelHelper.get();
            if (signup) {
                PEXServices.savePresentationLetter(signup).then((ret) => {
                    if (!ret.success) {
                        setTimeout(() => {
                            setInteractions([...interactions, {
                                author: InteractionAuthor.AMARELINHO,
                                text: ret.message
                            }]);
                            setTimeout(() => {
                                let chat = document.getElementsByClassName('chat')[0];
                                chat.scrollTo(0, chat.scrollHeight);
                            }, 500);
                        }, 1000)
                    } else {
                        TagManager.dataLayer(dataLayerHelper.formEnviado('form_idiomas'));
                        SignupModelHelper.update({ languages: null });
                        document.location = '#/carta-apresentacao';
                    }
                }).catch((ex) => {
                    setTimeout(() => {
                        setInteractions([...interactions, {
                            author: InteractionAuthor.AMARELINHO,
                            text: ex
                        }]);
                        setTimeout(() => {
                            let chat = document.getElementsByClassName('chat')[0];
                            chat.scrollTo(0, chat.scrollHeight);
                        }, 500);
                    }, 1000)
                });
            }
            if (signup) {
                await PEXServices.savePresentationLetter(signup);
                TagManager.dataLayer(dataLayerHelper.formEnviado('form_carta-de-apresentacao'));
                document.location = '/candidato/meu-curriculo.aspx';
            }
        }

        SignupModelHelper.update({
            presentationLetter: { description: letter }
        });

        savePresentationLetter();
    }


    function setPresentationLetter(e: any) {

        let textarea = document.getElementById('footer-input') as HTMLTextAreaElement;
        textarea.style.height = textarea.scrollHeight+'px';
        setLetter(e.target.value);
    }


    return (
        <>
            <Header step={1} total={1} title="Carta de apresentação" previous="#/idiomas-inicio"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString('Legal! Comece falando sobre o que você acha que tem de melhor, depois fale resumidamente o que você sabe fazer e finalize falando como você poderá ajudar a empresa com o seu trabalho. ')
                                                    .callFunction(() => {
                                                        setTyped(true);
                                                    })
                                                    .start();
                                            }}
                                        />
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="input-container d-flex align-items-center justify-content-start">
                                    <div className="input-container-group">
                                        <textarea 
                                            id="footer-input"
                                            contentEditable={true} 
                                            suppressContentEditableWarning={true} 
                                            onChange={(e) => setPresentationLetter(e)} 
                                            className="input-container-text"
                                            placeholder="Digite aqui"
                                            maxLength={countLetter}
                                            rows={1}
                                        ></textarea>
                                        <label className="input-count">Caracteres restantes: {countLetter - letter.length}</label>
                                    </div>
                                        {
                                            letter.length === 0 &&
                                            <button className="btn-submit" disabled={true}>
                                                <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                            </button>
                                        }
                                        {
                                            letter.length > 0 &&
                                            <button className="btn-submit" onClick={ () => checkPresentationLetter() }>
                                                <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                            </button>   
                                        }
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

        </>);
}
