import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import { JobObjectiveHelper, JobObjectiveModel, SignupModelHelper } from "../data/SignupModel";
import PEXServices from "../services/PEXServices";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

import Stack from '@mui/material/Stack';
import { CircularProgress } from '@mui/material';

export default function ObjetivosProfissionaisPesquisa(props: any) {

    const [typed, setTyped] = useState(false);
    const [jobFilter, setJobFilter] = useState('');
    const [jobList, setJobList] = useState<JobObjectiveModel[]>([]);
    const [loading, setLoading] = useState(false);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('pesquisa', 'objetivos-profissionais'));
    }, [])

    useEffect(() => {
        if (typed)
            document.getElementById('footer-input')?.focus();
    }, [typed]);

    async function setJob(job: JobObjectiveModel) {
        
        async function saveObjectives() {
            const signup = SignupModelHelper.get();
            if (signup) {
                
                await PEXServices.saveObjectives(signup);
                TagManager.dataLayer(dataLayerHelper.formEnviado('form_objetivos-profissionais'));

                switch (job.id) {
                    case "2602": // Motorista
                    case "2515": // Motoboy
                        document.location = '#/perguntas-motorista-trabalhar-noite';
                        break;
                    case "4419": // Costureir@
                        document.location = '#/perguntas-costureira-maquina';
                        break;
                    case "1061": // Manicure
                        document.location = '#/perguntas-manicure-tecnica';
                        break;
                    case "2549": // Vendedor
                        document.location = '#/perguntas-vendedor-tipo';
                        break;
                    case "1683": // Pintor
                        document.location = '#/perguntas-pintor-tecnica';
                        break;
                    default:
                        document.location = '#/experiencias-profissionais';
                        break;
                }

            }
        }

        SignupModelHelper.update({
            objectives: [job]
        });

        await saveObjectives();
    }

    // Originally inspired by  David Walsh (https://davidwalsh.name/javascript-debounce-function)
    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // `wait` milliseconds.
    const debounce = (func: any, wait: number) => {
        let timeout: NodeJS.Timeout;

        return function executedFunction(...args: any[]) {
            const later = () => {
                clearTimeout(timeout);
                func(...args);
            };

            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
        };
    };

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            const jobs = await PEXServices.getPositions(jobFilter);
            setLoading(false);

            if (jobs.success) {
                const allJobs = JobObjectiveHelper.fromList(jobs.items);
                setJobList(allJobs);
            }
        }

        fetchData();

    }, [jobFilter]);

    // if (!jobList || jobList.length === 0) {
    //     setJobList([
    //         { id: "2602", label: "Motorista", value: "Motoboy" },
    //         { id: "2515", label: "Motoboy", value: "Motoboy" },
    //         { id: "4419", label: 'Costureiro(a)', value: 'Costureiro(a)' },
    //         { id: "1061", label: "Manicure", value: "Manicure" },
    //         { id: "2549", label: "Vendedor", value: "Vendedor" },
    //         { id: "1683", label: "Pintor", value: "Pintor" }
    //     ]);
    // }

    let objJobs = jobList.filter((c: JobObjectiveModel) => {
        return c.label.toLowerCase().trim().normalize('NFD').replace(/[\u0300-\u036f]/g, '').indexOf(jobFilter.toLowerCase().trim().normalize('NFD').replace(/[\u0300-\u036f]/g, '')) > -1;
    }).map((c: JobObjectiveModel) => {
        return <div className="professional-item" key={jobList.indexOf(c)}>
            <button className="btn wrap btn-outline btn-full" data-id={c.id} onClick={() => setJob(c)}>{c.label}</button>
        </div>;
    }).slice(0, 6);


    return (
        <>
            <Header step={2} total={2} title="Seu objetivo profissional" previous="#/objetivos-profissionais" finished={false}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(5).typeString(`Do que você quer trabalhar?`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {loading &&
                            <>
                                <Stack sx={{ color: '#6F5192' }} justifyContent="center" alignItems="center">
                                    <CircularProgress color="inherit" />
                                </Stack>
                            </>
                        }
                        {typed && !loading && objJobs &&
                            <div className="professional-list">
                                {
                                    objJobs.map((objJob: any) => {
                                        return objJob;
                                    })
                                }
                            </div>
                        }

                    </div>
                </div>

                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="footer-answer-holder d-flex align-items-center justify-content-between">
                                    <input type="text" id="footer-input" placeholder="Pesquise uma profissão" onChange={(e) => debounce(setJobFilter(e.target.value), 500)} />
                                    {
                                        jobFilter.length === 0 &&
                                        <button className="btn-submit" disabled={true}>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M17.1527 15.0943H16.0686L15.6844 14.7238C17.0292 13.1595 17.8388 11.1286 17.8388 8.91938C17.8388 3.99314 13.8456 0 8.91938 0C3.99314 0 0 3.99314 0 8.91938C0 13.8456 3.99314 17.8388 8.91938 17.8388C11.1286 17.8388 13.1595 17.0292 14.7238 15.6844L15.0943 16.0686V17.1527L21.9554 24L24 21.9554L17.1527 15.0943ZM8.91938 15.0943C5.50257 15.0943 2.74443 12.3362 2.74443 8.91938C2.74443 5.50257 5.50257 2.74443 8.91938 2.74443C12.3362 2.74443 15.0943 5.50257 15.0943 8.91938C15.0943 12.3362 12.3362 15.0943 8.91938 15.0943Z" fill="#6F5192" />
                                            </svg>
                                        </button>
                                    }
                                    {
                                        jobFilter.length > 0 &&
                                        <button className="btn-submit" onClick={() => setJobFilter(jobFilter)}>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M17.1527 15.0943H16.0686L15.6844 14.7238C17.0292 13.1595 17.8388 11.1286 17.8388 8.91938C17.8388 3.99314 13.8456 0 8.91938 0C3.99314 0 0 3.99314 0 8.91938C0 13.8456 3.99314 17.8388 8.91938 17.8388C11.1286 17.8388 13.1595 17.0292 14.7238 15.6844L15.0943 16.0686V17.1527L21.9554 24L24 21.9554L17.1527 15.0943ZM8.91938 15.0943C5.50257 15.0943 2.74443 12.3362 2.74443 8.91938C2.74443 5.50257 5.50257 2.74443 8.91938 2.74443C12.3362 2.74443 15.0943 5.50257 15.0943 8.91938C15.0943 12.3362 12.3362 15.0943 8.91938 15.0943Z" fill="#fff" />
                                            </svg>
                                        </button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        </>);
}
