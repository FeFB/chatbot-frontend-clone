import { useEffect } from "react";
import TagManager from "react-gtm-module";
import Header from "../components/header";
import { dataLayerHelper } from "../data/dataLayerModel";
import { SignupModelHelper } from "../data/SignupModel";

export default function Intro(props: any) {

    useEffect( () => {
        TagManager.dataLayer(dataLayerHelper.pagina('intro', 'cadastro-usuario'));
    }, [])

    function goToNome() {
        SignupModelHelper.reset();
        document.location = '#/nome';
    }
    return (
        <>
            {/* <div className="fixHeight"> */}
                <Header total={17} step={1} title={"Bem-vindo (a)"} previous="/"></Header>

                <div className="chat-container">

                    <div className="chat">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">
                                    <div className="chat-item d-flex align-items-start justify-content-start">
                                        <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                        <div className="chat-item-content">
                                            <p>Que bom ter você aqui!<br />
                                                Quero te ajudar a conseguir sua vaga de emprego.</p>

                                            <p><strong>Vamos criar sua conta juntos?</strong><br />
                                                É bem simples.</p>
                                            <p>Eu vou perguntar algumas coisas e você vai me respondendo. Quando terminar, <strong>você terá um currículo bem completo</strong> para apresentar.</p>
                                            <p>Vamos lá?</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <footer className="footer-fixed">
                        <div className="container">
                            <div className="row d-flex align-items-center justify-content-center">
                                <div className="col-12 col-md-4">
                                    <button className="btn btn-secondary btn-full" onClick={() => goToNome()}>começar</button>
                                </div>
                            </div>
                        </div>
                    </footer>

                </div>
            {/* </div> */}
        </>);
}
