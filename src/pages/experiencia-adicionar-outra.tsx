import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import { JobExperiencesModel, SignupModelHelper } from "../data/SignupModel";
import PEXService from "../services/PEXServices";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function ExperienciaAdicionarOutra(props: any) {

    const [typed, setTyped] = useState(false);
    // const [another, setAnother] = useState<null | boolean>(null);
    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [experience, setExperience] = useState<JobExperiencesModel | null>(null);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });

        let signup = SignupModelHelper.get();
        if (!signup) {
            document.location = '#/experiencias-profissionais';
        }

        setExperience(SignupModelHelper.getOngoingExperience());
        
        TagManager.dataLayer(dataLayerHelper.pagina('adicionar-outra', 'experiencia'));
    }, [])

    function changeAnother(a:boolean) {

        let signup = SignupModelHelper.get();

        if (a === false) {
            const chat = [...interactions, {
                text: 'Cadastrar',
                author: InteractionAuthor.CLIENTE
            }];

            if (signup) {
                signup = SignupModelHelper.updateExperience({ saved: true });
                PEXService.saveExperiences(signup).then((ret) => {
                    if (!ret.success) {
                        setTimeout(() => {
                            setInteractions([...chat, {
                                author: InteractionAuthor.AMARELINHO,
                                text: ret.message
                            }]);
                            setTimeout(() => {
                                let chat = document.getElementsByClassName('chat')[0];
                                chat.scrollTo(0, chat.scrollHeight);
                            }, 500);
                        }, 1000)
                    } else {
                        
                        TagManager.dataLayer(dataLayerHelper.formEnviado('form_experiencias-profissionais'));
                        SignupModelHelper.update({ experiences: null });
                        document.location = '#/escolaridade-inicio';
                    }
                }).catch((ex) => {
                    setTimeout(() => {
                        setInteractions([...chat, {
                            author: InteractionAuthor.AMARELINHO,
                            text: ex
                        }]);
                        setTimeout(() => {
                            let chat = document.getElementsByClassName('chat')[0];
                            chat.scrollTo(0, chat.scrollHeight);
                        }, 500);
                    }, 1000)
                });
            }
        }
        if (a === true) {
            SignupModelHelper.updateExperience({ saved: true });
            document.location = '#/experiencia-cargo';
        }
    };


    let objItemsList: any = [
        { label: "Adicionar outra experiência", value: true },
        { label: "Deixar para depois", value: false }
    ];

    let objItems = objItemsList.map((c: any) => {
        return <div className="professional-item" key={objItemsList.indexOf(c)}>
            <button className="btn btn-outline" onClick={() => changeAnother(c.value)}>{c.label}</button>
        </div>;
    });

    let arrMonths: string[] = ['', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

    return (
        <>
            <Header step={10} total={10} title="Adicionar experiência" previous={experience?.current ? "#/experiencia-entrada-mes" : "#/experiencia-saida-mes"} finished={false}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>
                                            {experience?.current === true && `${arrMonths[new Date(experience?.start).getMonth() + 1]} de ${new Date(experience?.start).getFullYear()}`}
                                            {experience?.current === false && `${arrMonths[new Date(experience?.finish).getMonth() + 1]} de ${new Date(experience?.finish).getFullYear()}`}
                                        </p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(15).typeString(`Você pode <strong>adicionar outras experiências agora ou depois</strong>.
                                                E caso não possua mais experiências, clique em “Deixar para Depois”.`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }}
                                        />
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }
                            </div>
                        </div>
                        {typed &&
                            <div className="professional-list">
                                {
                                    objItems.map((objItem: any) => {
                                        return objItem;
                                    })
                                }
                            </div>
                        }
                    </div>
                </div>
            </div>
        </>);
}

