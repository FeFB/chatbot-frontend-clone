import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import { SignupModel, SignupModelHelper } from "../data/SignupModel";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function ExperienciaPossui(props: any) {

    const [typed, setTyped] = useState(false);
    const [experienced, setExperienced] = useState<boolean | null>(null);
    const [signup, setSignup] = useState<SignupModel | null>(null);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('experiencias-profissionais', 'experiencia'));
        setSignup(SignupModelHelper.get());
    }, []);

    useEffect(() => {
        if (experienced === false) {
            document.location = '#/experiencia-possui-nao';
        }
        if (experienced === true) {
            document.location = '#/experiencia-cargo';
        }
    }, [experienced]);


    let objOptionsList: any = [
        { label: "Sim, já tenho", value: true },
        { label: "Ainda não", value: false }
    ];

    let objJobs = objOptionsList.map((c: any) => {
        return <div className="professional-item" key={objOptionsList.indexOf(c)}>
            <button className="btn btn-outline" onClick={() => setExperienced(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={1} total={10} title="Sua experiência" previous="#/objetivos-profissionais" finished={false}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                {signup && signup.objectives &&
                                    <div className="chat-item chat-item-user">
                                        <img src="images/ico-chat-user.svg" alt="Usuário" />
                                        <div className="chat-item-content">
                                            <p>{signup?.objectives?.map(o => o.label).join(', ')}</p>
                                        </div>
                                    </div>
                                }

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(5)
                                                    .typeString(`Legal!<br/>E você <strong>já tem alguma experiência</strong> profissional?`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {typed &&
                            <div className="professional-list">
                                {
                                    objJobs.map((objCity: any) => {
                                        return objCity;
                                    })
                                }
                            </div>
                        }

                    </div>
                </div>
            </div>
        </>);
}
