import { useState, useEffect } from "react";
import Header from "../components/header";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import { AddressModel, SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import ValidationService from "../services/PEXServices";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function Cep(props: any) {

    const [cep, setCep] = useState('');
    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [birthMonthName, setBirthMonthName] = useState('');
    const [birthDayNumber, setBirthDayNumber] = useState(0);
    const [birthYearNumber, setBirthYearNumber] = useState(0);
    const [typed, setTyped] = useState(false);
    const [options, setOptions] = useState(false);


    useEffect(() => {
        setTyped(false)
        TagManager.dataLayer(dataLayerHelper.pagina('cep', 'cadastro-usuario'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setBirthYearNumber(signup.birthYearNumber);
            setBirthMonthName(signup.birthMonth);
            setBirthDayNumber(signup.birthDayNumber);
        } else {
            document.location = './';
        }
    }, [])

    useEffect(() => {
        if (typed)
            document.getElementById('footer-input')?.focus();
    }, [typed]);

    async function checkCEP() {
        setTyped(false)
        const cepLimpo = String(cep)
            .toLowerCase()
            .replace(/-/g, '').replace(/\./g, '').trim();

        if (cepLimpo.length !== 8) {
            const chat = [...interactions, {
                text: cep,
                author: InteractionAuthor.CLIENTE
            }];
            setInteractions(chat);

            setTimeout(() => {
                setInteractions([...chat, {
                    author: InteractionAuthor.AMARELINHO,
                    text: 'Este CEP não é válido.  Pode tentar novamente por favor?'
                }]);
                setTimeout(() => {
                    let chat = document.getElementsByClassName('chat')[0];
                    chat.scrollTo(0, chat.scrollHeight);
                }, 500);
            }, 1000)
        } else {

            const ret = await ValidationService.getAddressByZipCode(cepLimpo);

            if (!ret.success) {
                const chat = [...interactions, {
                    text: cepLimpo,
                    author: InteractionAuthor.CLIENTE
                }];
                setInteractions(chat);

                setTimeout(() => {
                    setInteractions([...chat, {
                        author: InteractionAuthor.AMARELINHO,
                        text: ret.message ? ret.message : 'CEP não encontrado.'
                    }]);
                    setTimeout(() => {
                        let chat = document.getElementsByClassName('chat')[0];
                        chat.scrollTo(0, chat.scrollHeight);
                    }, 500);
                }, 1000)

            } else {

                const objAddress = {
                    zipCode: cepLimpo,
                    street: ret.logradouro,
                    state: ret.uf,
                    city: ret.cidade,
                    neighborhood: ret.bairro,
                    typedStreet: ret.logradouro ? false : true,
                } as AddressModel;
                SignupModelHelper.update({ address: objAddress });

                if (objAddress.street)
                    document.location = '#/endereco-numero';
                else
                    document.location = '#/endereco-logradouro';
            }
        }
    }

    return (
        <>
            <Header step={8} total={17} title="Seu endereço" previous="#/nascimento-ano"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{`${birthDayNumber} de ${birthMonthName} de ${birthYearNumber}`}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(15).typeString(`Pronto!<br />
                                                Preciso do seu <strong>CEP</strong>.`)
                                                .callFunction(() => {
                                                    setTyped(true);
                                                    setOptions(true);
                                                })
                                                    .start();
                                            }} />
                                    </div>
                                </div>
                                
                                { options &&
                                <div className="chat-item p-0">
                                    <a className="btn btn-outline btn-full" rel="noreferrer" href="https://buscacepinter.correios.com.br/app/endereco/index.php" target={'_blank'}>Não sei meu CEP!</a>
                                </div>
                                }

                                {
                                    interactions.map((interaction) => {
                                        let ret;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            ret = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                            .callFunction(() => {
                                                                setTyped(true)
                                                            })
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            ret = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return ret;
                                    })
                                }

                            </div>

                        </div>
                    </div>
                </div>

                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="footer-answer-holder d-flex align-items-center justify-content-between">
                                    <input type="text" name="" id="footer-input" maxLength={9} placeholder="Digite seu cep" onChange={(e) => setCep(e.target.value)} onKeyUp={(ev) => { if (ev.key === 'Enter') checkCEP() }} />
                                    {
                                        cep.length === 0 &&
                                        <button className="btn-submit" disabled={true}>
                                            <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                        </button>
                                    }
                                    {
                                        cep.length > 0 &&
                                        <button className="btn-submit" onClick={() => checkCEP()}>
                                            <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                        </button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        </>);
}




