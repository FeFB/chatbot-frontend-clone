import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import { GraduationModel, SignupModelHelper } from "../data/SignupModel";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function EscolaridadeAindaEstuda(props: any) {

    const [typed, setTyped] = useState(false);
    const [current, setCurrent] = useState<null | boolean>(null);
    const [graduation, setGraduation] = useState<GraduationModel | null>(null);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('ainda-estuda', 'escolaridade'));
        setGraduation(SignupModelHelper.getOngoingGraduation());
    }, []);


    useEffect(() => {
        if (current === false || current === true) {
            SignupModelHelper.updateGraduation({ current });
            document.location = '#/escolaridade-inicio-ano';
        }
    }, [current]);


    let objItemsList: any = [
        { label: "Sim, estou cursando", value: true },
        { label: "Não, já terminei", value: false }
    ];

    let objItems = objItemsList.map((c: any) => {
        return <div className="professional-item" key={objItemsList.indexOf(c)}>
            <button className="btn btn-outline" onClick={() => setCurrent(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={4} total={8} title="Escolaridade" previous={`#/escolaridade-${graduation?.course ? 'curso' : 'instituicao'}`} finished={false}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{graduation?.course || graduation?.name}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(15).typeString(`Você <strong>ainda está cursando?</strong>`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {typed &&
                            <div className="professional-list">
                                {
                                    objItems.map((objItem: any) => {
                                        return objItem;
                                    })
                                }
                            </div>
                        }
                    </div>
                </div>
            </div>
        </>);
}

