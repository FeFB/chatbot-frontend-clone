import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function PerguntasMotoristaHabilitacao(props: any) {

    const [workAtWeekend, setWorkAtWeekend] = useState(false);
    const [activeA, setActiveA] = useState(false);
    const [activeB, setActiveB] = useState(false);
    const [activeC, setActiveC] = useState(false);
    const [activeD, setActiveD] = useState(false);
    const [activeE, setActiveE] = useState(false);
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('trabalhar-habilitacao', 'perguntas-especificas-motorista'));

        const signup = SignupModelHelper.getOngoingDriverQuestions();
        if (signup) {
            setWorkAtWeekend(signup.workWeekend);
        } else {
            document.location = './#/perguntas-motorista-trabalhar-noite';
        }
    }, [])

    function activeOption(license: string) {
        switch (license) {
            case 'A':
                setActiveA(!activeA);
                break;
            case 'B':
            case 'C':
            case 'D':
            case 'E':
                setActiveB(license === 'B' ? !activeB : false);
                setActiveC(license === 'C' ? !activeC : false);
                setActiveD(license === 'D' ? !activeD : false);
                setActiveE(license === 'E' ? !activeE : false);
                break;
        }
    }

    function getLicense(): string {

        let l = '';
        if (activeA)
            l = 'A';
        if (activeB)
            l += 'B';
        else if (activeC)
            l += 'C';
        else if (activeD)
            l += 'D';
        else if (activeE)
            l += 'E';

        return l;
    }

    function setOption() {
        SignupModelHelper.updateDriverQuestions({ license: getLicense() });
        document.location = '#/perguntas-motorista-veiculo';
    }

    let optionItemsList: any = [
        { label: "Categoria A", value: 'A' },
        { label: "Categoria B", value: 'B' },
        { label: "Categoria C", value: 'C' },
        { label: "Categoria D", value: 'D' },
        { label: "Categoria E", value: 'E' },
    ];

    let optionItems = optionItemsList.map((c: any) => {

        let active = activeA;
        if (c.value === 'B')
            active = activeB;
        else if (c.value === 'C')
            active = activeC;
        else if (c.value === 'D')
            active = activeD;
        else if (c.value === 'E')
            active = activeE;

        return <div className="col-6 col-md-4" key={optionItemsList.indexOf(c)}>
            <div className="option-item mt-3">
                <button className={`btn btn-outline btn-full ${active ? 'active' : ''}`} onClick={() => activeOption(c.value)}>{c.label}</button>
            </div>
        </div>;
    });

    return (
        <>
            <Header step={3} total={7} title="Motoristas ou Motoboys" previous="#/perguntas-motorista-trabalhar-finaldesemana"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{workAtWeekend ? 'Posso trabalhar' : 'Não tenho disponibilidade'}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Me diz <strong>qual a categoria da sua habilitação?</strong> <br>Pode escolher mais  de uma.`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {typed &&
                                    <div className="option-list row">
                                        {
                                            optionItems.map((objItem: any) => {
                                                return objItem;
                                            })
                                        }
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-fixed">
                    <div className="container">
                        <div className="row d-flex align-items-center justify-content-center">
                            <div className="col-12 col-md-4">
                                <button className="btn btn-secondary btn-full" disabled={getLicense() === ''} onClick={() => setOption()}>Continuar</button>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

        </>);
}
