import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import SurveyServices from "../services/SurveyServices";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";

export default function PerguntasManicureMaterial(props: any) {

    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [mei, setMei] = useState(false);
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('material', 'perguntas-especificas-manicure'));

        const signup = SignupModelHelper.getOngoingManicureQuestions();
        if (signup) {
            setMei(signup.mei);
        } else {
            document.location = './';
        }
    }, [])

    function setOption(material: boolean) {
        SignupModelHelper.updateManicureQuestions({ material });

        const signup = SignupModelHelper.get();
        if (signup) {
            SurveyServices.saveManicureQuestion(signup).then((ret) => {
                if (!ret.success) {
                    setTimeout(() => {
                        setInteractions([...interactions, {
                            author: InteractionAuthor.AMARELINHO,
                            text: ret.message
                        }]);
                        setTimeout(() => {
                            let chat = document.getElementsByClassName('chat')[0];
                            chat.scrollTo(0, chat.scrollHeight);
                        }, 500);
                    }, 1000)
                } else {
                    SignupModelHelper.update({ manicureQuestions: null });
                    TagManager.dataLayer(dataLayerHelper.formEnviado('form_perguntas-especificas-manicure'));
                    document.location = '#/experiencias-profissionais';
                }
            }).catch((ex) => {
                setTimeout(() => {
                    setInteractions([...interactions, {
                        author: InteractionAuthor.AMARELINHO,
                        text: ex
                    }]);
                    setTimeout(() => {
                        let chat = document.getElementsByClassName('chat')[0];
                        chat.scrollTo(0, chat.scrollHeight);
                    }, 500);
                }, 1000)
            });
        }
    }

    let optionItemsList: any = [
        { label: "Sim, trabalho com meus material", value: true },
        { label: "Não tenho", value: false },
    ];

    let optionItems = optionItemsList.map((c: any) => {
        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className="btn btn-outline btn-full" onClick={() => setOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={3} total={3} title="Manicure" previous="#/perguntas-manicure-mei"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{mei ? 'Sim, tenho MEI' : 'Não tenho MEI'}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`E <strong>material próprio</strong>, você tem?`)
                                                .callFunction(() => {
                                                    setTyped(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">                                                    
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
