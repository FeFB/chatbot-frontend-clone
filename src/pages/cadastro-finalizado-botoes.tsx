import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function Finalizado(props: any) {

    const [fillCV, setFillCV] = useState<null | boolean>(null);
    const [typed, setTyped] = useState(false);

    useEffect(() => {
        TagManager.dataLayer(dataLayerHelper.pagina('finalizado', 'cadastro-usuario'));
    }, [])

    useEffect(() => {
        if (fillCV === true) {
            TagManager.dataLayer(dataLayerHelper.pagina('finalizado-preencher', 'cadastro-usuario'));
            document.location = '#/objetivos-profissionais';
        }
        if (fillCV === false) {
            TagManager.dataLayer(dataLayerHelper.pagina('finalizado-nao-preencher', 'cadastro-usuario'));
            document.location = '/candidato/home.aspx';
        }
    }, [fillCV])


    return (
        <>
            <Header step={17} total={17} title="Cadastro finalizado" previous="#" finished={true}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter.changeDelay(5).typeString(`Prontinho!<br />
                                                    <strong>Sua conta está criada!</strong><br/>
                                                    Agora vamos preencher seu currículo juntos?`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {typed &&
                                    <div className="gender-list">
                                        <div className="gender-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setFillCV(true)}>Preencher currículo</button>
                                        </div>
                                        <div className="gender-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setFillCV(false)}>Agora não</button>
                                        </div>
                                    </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </>);
}
