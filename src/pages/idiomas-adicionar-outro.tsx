import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import { LanguageModel, SignupModelHelper } from "../data/SignupModel";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";
import PEXServices from "../services/PEXServices";

export default function IdiomasAdicionarOutro(props: any) {

    const [typed, setTyped] = useState(false);
    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [language, setLanguage] = useState<LanguageModel | null>(null);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });

        let signup = SignupModelHelper.get();
        if (!signup) {
            document.location = '#/idiomas-inicio';
        }

        setLanguage(SignupModelHelper.getOngoingLanguage());
        
        TagManager.dataLayer(dataLayerHelper.pagina('adicionar-outro', 'idiomas'));
    }, [] )

    function changeAnother(a:boolean) {

        let signup = SignupModelHelper.get();

        if (a === false) {
            const chat = [...interactions, {
                text: 'Cadastrar',
                author: InteractionAuthor.CLIENTE
            }];

            signup = SignupModelHelper.updateLanguage({ saved: true });

            if (signup) {
                PEXServices.saveLanguages(signup).then((ret) => {
                    if (!ret.success) {
                        setTimeout(() => {
                            setInteractions([...chat, {
                                author: InteractionAuthor.AMARELINHO,
                                text: ret.message
                            }]);
                            setTimeout(() => {
                                let chat = document.getElementsByClassName('chat')[0];
                                chat.scrollTo(0, chat.scrollHeight);
                            }, 500);
                        }, 1000)
                    } else {
                        TagManager.dataLayer(dataLayerHelper.formEnviado('form_idiomas'));
                        SignupModelHelper.update({ languages: null });
                        document.location = '#/carta-apresentacao';
                    }
                }).catch((ex) => {
                    setTimeout(() => {
                        setInteractions([...chat, {
                            author: InteractionAuthor.AMARELINHO,
                            text: ex
                        }]);
                        setTimeout(() => {
                            let chat = document.getElementsByClassName('chat')[0];
                            chat.scrollTo(0, chat.scrollHeight);
                        }, 500);
                    }, 1000)
                });
            }
        }
        else if (a === true) {
            SignupModelHelper.updateLanguage({ saved: true });
            document.location = '#/idiomas-inicio';
        }
    }


    let objItemsList: any = [
        { label: "Adicionar outro idioma", value: true },
        { label: "Deixar para depois", value: false }
    ];

    let objItems = objItemsList.map((c: any) => {
        return <div className="professional-item" key={objItemsList.indexOf(c)}>
            <button className="btn btn-outline" onClick={() => changeAnother(c.value)}>{c.label}</button>
        </div>;
    });

    const arrLevels = [
        'Básico',
        'Intermediário',
        'Fluente',
        'Nativo'
    ]

    return (
        <>
            <Header step={4} total={4} title="Idiomas" previous="#/idiomas-nivel" finished={false}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        {language && arrLevels[language.level]}
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Quer adicionar <strong>outros idiomas</strong>?`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }}
                                        />
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let objRet;

                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            objRet = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter
                                                                .changeDelay(15)
                                                                .typeString(interaction.text)
                                                                .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            objRet = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }

                                        return objRet;
                                    })
                                }
                            </div>
                        </div>
                        {typed &&
                            <div className="professional-list">
                                {
                                    objItems.map((objItem: any) => {
                                        return objItem;
                                    })
                                }
                            </div>
                        }
                    </div>
                </div>
            </div>
        </>);
}

