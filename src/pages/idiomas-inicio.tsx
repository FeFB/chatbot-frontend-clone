import { useEffect, useState } from "react";
import Header from "../components/header";
import { LanguageType, SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function IdiomasInicio(props: any) {

    const [typed, setTyped] = useState(false);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('inicio', 'idiomas'));
    }, [])

    function setLanguage(language: LanguageType | null) {

        if (language !== null) {
            SignupModelHelper.updateLanguage({ type: language });

            if (language === LanguageType.OUTRO) {
                document.location = '#/idiomas-outro';
            } else {
                document.location = '#/idiomas-nivel';
            }

        } else {
            document.location = '#/idiomas-apenas-portugues';
        }
    }

    // let hidePtBR = false;
    // const model = SignupModelHelper.get();
    // if (model && model.languages && model.languages.length > 0) hidePtBR = true;

    return (
        <>
            <Header step={1} total={4} title="Idiomas" previous="#/cursos-inicio"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString(`E você <strong>entende outro idioma</strong>?`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                    </div>
                                </div>

                                {typed &&
                                    <div className="professional-list">
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguage(LanguageType.INGLES)}>Inglês</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguage(LanguageType.ESPANHOL)}>Espanhol</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguage(LanguageType.FRANCES)}>Francês</button>
                                        </div>
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguage(LanguageType.ALEMAO)}>Alemão</button>
                                        </div>
                                        {/* <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguage(LanguageType.OUTRO)}>Outro</button>
                                        </div> */}
                                        <div className="professional-item">
                                            <button className="btn btn-outline btn-full" onClick={() => setLanguage(null)}>Não, apenas o português</button>
                                        </div>
                                    </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>);
}
