import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function PerguntasMotoristaVeiculoBau(props: any) {

    const [vehicle, setVehicle] = useState('');
    const [typed, setTyped] = useState(false);
    const [backVehicle, setBackVehicle] = useState(true);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('veiculo-bau', 'perguntas-especificas-motorista'));

        const signup = SignupModelHelper.get();
        const objectiveId = signup?.objectives[ signup?.objectives.length-1 ].id;
        if ( objectiveId === '2515' ) { // MOTOBOY
            setBackVehicle(false);
        }

        const driverQuestions = SignupModelHelper.getOngoingDriverQuestions();

        if (driverQuestions) {
            setVehicle(driverQuestions.vehicle);
        } else {
            document.location = './#/perguntas-motorista-veiculo';
        }
    }, [])

    function setOption(vehicleTrunk: string) {
        SignupModelHelper.updateDriverQuestions({ vehicleTrunk })
        document.location = '#/perguntas-motorista-veiculo-ano';
    }

    let optionItemsList: any = [
        { label: "Sim, tem baú", value: true },
        { label: "Não possuo", value: false },
    ];

    let optionItems = optionItemsList.map((c: any) => {
        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className="btn btn-outline btn-full" onClick={() => setOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={5} total={7} title="Motoristas ou Motoboys" previous={`#/perguntas-motorista-${backVehicle ? 'veiculo' : 'habilitacao'}`}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{'Moto'}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <p>
                                            <Typewriter
                                                onInit={(typewriter) => {
                                                    typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Possuí <strong>baú</strong>?`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }} />
                                        </p>
                                    </div>
                                </div>

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </>);
}
