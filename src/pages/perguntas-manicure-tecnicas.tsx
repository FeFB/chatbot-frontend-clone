import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function PerguntasManicureTecnica(props: any) {

    const [name, setName] = useState('');
    const [step, setStep] = useState(false);
    const [typed, setTyped] = useState(false);
    const [activeA, setActiveA] = useState(false);
    const [activeB, setActiveB] = useState(false);
    const [activeC, setActiveC] = useState(false);
    const [activeD, setActiveD] = useState(false);
    const [activeE, setActiveE] = useState(false);
    const [activeF, setActiveF] = useState(false);
    const [activeG, setActiveG] = useState(false);
    const [activeH, setActiveH] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('tecnica', 'perguntas-especificas-manicure'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setName(signup.name);
        } else {
            document.location = './';
        }
    }, [])

    function activeOption(maquina: string) {

        if ( maquina!='H' )
            setActiveH(false);

        switch (maquina) {
            case 'A':
                setActiveA(!activeA);
                break;
            case 'B':
                setActiveB(!activeB);
                break;
            case 'C':
                setActiveC(!activeC);
                break;
            case 'D':
                setActiveD(!activeD);
                break;
            case 'E':
                setActiveE(!activeE);
                break;
            case 'F':
                setActiveF(!activeF);
                break;
            case 'G':
                setActiveG(!activeG);
                break;

            case 'H':
                setActiveA(false);
                setActiveB(false);
                setActiveC(false);
                setActiveD(false);
                setActiveE(false);
                setActiveF(false);
                setActiveG(false);
                setActiveH(!activeH);
                break;
        }
    }

    function getTecnicas() {
        
        let r = '';
        if (activeA)
            r = 'Unha de Gel';
        if (activeB)
            r += (r!=='' ? ', ' : '') + 'Porcelana';
        if (activeC)
            r += (r!=='' ? ', ' : '') + 'Acrílico';
        if (activeD)
            r += (r!=='' ? ', ' : '') + 'Acrigel';
        if (activeE)
            r += (r!=='' ? ', ' : '') + 'Fibra de Vidro';
        if (activeF)
            r += (r!=='' ? ', ' : '') + 'Fibra de Seda';
        if (activeG)
            r += (r!=='' ? ', ' : '') + 'Postiças';
        if (activeH)
            r += (r!=='' ? ', ' : '') + 'Nenhuma';

        return r;
    }

    function setOption(technique: string) {
        SignupModelHelper.updateManicureQuestions({ technique });
        document.location = '#/perguntas-manicure-mei';
    }

    let optionItemsList: any = [
        { label: "Unha de Gel", value: 'A' },
        { label: "Porcelana", value: 'B' },
        { label: "Acrílico", value: 'C' },
        { label: "Acrigel", value: 'D' },
        { label: "Fibra de Vidro", value: 'E' },
        { label: "Fibra de Seda", value: 'F' },
        { label: "Postiças", value: 'G' },
        { label: "Nenhuma", value: 'H' },
    ];
    
    let optionItems = optionItemsList.map((c: any) => {

        let active = activeA;
        if (c.value === 'B')
            active = activeB;
        else if (c.value === 'C')
            active = activeC;
        else if (c.value === 'D')
            active = activeD;
        else if (c.value === 'E')
            active = activeE;
        else if (c.value === 'F')
            active = activeF;
        else if (c.value === 'G')
            active = activeG;
        else if (c.value === 'H')
            active = activeH;

        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className={`btn btn-outline btn-full ${active ? 'active' : ''}`} onClick={() => activeOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={1} total={3} title="Manicure" previous="#/objetivos-profissionais"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user d-none">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{name}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`Vamos lá. Agora eu preciso fazer algumas perguntas sobre o seu cargo de interesse.`)
                                                .callFunction(() => {
                                                    setStep(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>
                                
                                { step &&
                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                .changeDelay(15)
                                                .typeString(`Quais as <strong>técnicas que você sabe fazer?</strong> <br>Escolha quantas souber.`)
                                                .callFunction(() => {
                                                    setTyped(true)
                                                })
                                                .start();
                                        }} />
                                    </div>
                                </div>
                                }

                                { typed &&
                                <div className="option-list">
                                    {
                                        optionItems.map((objItem: any) => {
                                            return objItem;
                                        })
                                    }
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-fixed">
                    <div className="container">
                        <div className="row d-flex align-items-center justify-content-center">
                            <div className="col-12 col-md-4">
                                <button className="btn btn-secondary btn-full" disabled={ getTecnicas()==='' ? true : false } onClick={() => setOption( getTecnicas() )}>Continuar</button>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

        </>);
}
