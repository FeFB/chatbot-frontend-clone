import Header from "../components/header";
import Typewriter from 'typewriter-effect';
import { useEffect, useState } from "react";
import { JobExperiencesModel, SignupModelHelper } from "../data/SignupModel";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function ExperienciaAindaTrabalha(props: any) {

    const [typed, setTyped] = useState(false);
    const [current, setCurrent] = useState<null | boolean>(null);
    const [experience, setExperience] = useState<JobExperiencesModel | null>(null);

    useEffect( () => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('ainda-trabalha', 'experiencia'));
    }, [])

    useEffect(() => {
        setExperience(SignupModelHelper.getOngoingExperience());
        if (current === false || current === true) {
            SignupModelHelper.updateExperience({ current });
            document.location = '#/experiencia-entrada-ano';
        }
    }, [current]);


    let objItemsList: any = [
        { label: "Sim, é meu cargo atual", value: true },
        { label: "Não, já sai dessa empresa", value: false }
    ];

    let objItems = objItemsList.map((c: any) => {
        return <div className="professional-item" key={objItemsList.indexOf(c)}>
            <button className="btn btn-outline" onClick={() => setCurrent(c.value)}>{c.label}</button>
        </div>;
    });


    return (
        <>
            <Header step={5} total={10} title="Adicionar experiência" previous="#/experiencia-atividades" finished={false}></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{experience?.activities}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <Typewriter
                                            onInit={(typewriter) => {
                                                typewriter
                                                    .changeDelay(15)
                                                    .typeString(`Você <strong>ainda trabalha nessa empresa?</strong>`)
                                                    .callFunction(() => {
                                                        setTyped(true)
                                                    })
                                                    .start();
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {typed &&
                            <div className="professional-list">
                                {
                                    objItems.map((objItem: any) => {
                                        return objItem;
                                    })
                                }
                            </div>
                        }
                    </div>
                </div>
            </div>
        </>);
}

