import { useEffect, useState } from "react";
import Header from "../components/header";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function PerguntasMotoristaVeiculoCarga(props: any) {

    const [vehicle, setVehicle] = useState('');
    const [typed, setTyped] = useState(false);
    const [activeA, setActiveA] = useState(false);
    const [activeB, setActiveB] = useState(false);
    const [activeC, setActiveC] = useState(false);
    const [activeD, setActiveD] = useState(false);
    const [activeE, setActiveE] = useState(false);
    const [activeF, setActiveF] = useState(false);
    const [activeG, setActiveG] = useState(false);
    const [activeH, setActiveH] = useState(false);
    const [activeI, setActiveI] = useState(false);
    const [activeJ, setActiveJ] = useState(false);

    useEffect(() => {
        SignupModelHelper.checkLogin().then(async(userId) => {
            if ( !userId ) {
                document.location = '#/';
            }
        });
        
        TagManager.dataLayer(dataLayerHelper.pagina('veiculo-carga', 'perguntas-especificas-motorista'));

        const signup = SignupModelHelper.getOngoingDriverQuestions();
        if (signup) {
            setVehicle(signup.vehicle);
        } else {
            document.location = './#/perguntas-motorista-veiculo';
        }
    }, [])

    function activeOption(license: string) {
        switch (license) {
            case 'A':
                setActiveA(!activeA);
                break;
            case 'B':
                setActiveB(!activeB);
                break;
            case 'C':
                setActiveC(!activeC);
                break;
            case 'D':
                setActiveD(!activeD);
                break;
            case 'E':
                setActiveE(!activeE);
                break;
            case 'F':
                setActiveF(!activeF);
                break;
            case 'G':
                setActiveG(!activeG);
                break;
            case 'H':
                setActiveH(!activeH);
                break;
            case 'I':
                setActiveI(!activeI);
                break;
            case 'J':
                setActiveJ(!activeJ);
                break;
        }
    }

    function getCargo(): string {

        let c = '';
        if (activeA)
            c = 'Cargas frigoríficas';
        if (activeB)
            c += (c !== '' ? ', ' : '') + 'Cargas a granel';
        if (activeC)
            c += (c !== '' ? ', ' : '') + 'Cargas vivas';
        if (activeD)
            c += (c !== '' ? ', ' : '') + 'Cargas de grande porte';
        if (activeE)
            c += (c !== '' ? ', ' : '') + 'Cargas de valor';
        if (activeF)
            c += (c !== '' ? ', ' : '') + 'Cargas secas';
        if (activeG)
            c += (c !== '' ? ', ' : '') + 'Cargas perigosas';
        if (activeH)
            c += (c !== '' ? ', ' : '') + 'Cargas de minério e cimento';
        if (activeI)
            c += (c !== '' ? ', ' : '') + 'Cargas frágeis';
        if (activeJ)
            c += (c !== '' ? ', ' : '') + 'Cargas de medicamentos';

        return c;
    }

    function setOption() {
        SignupModelHelper.updateDriverQuestions({ vehicleCargo: getCargo() });
        document.location = '#/perguntas-motorista-veiculo-mopp';
    }

    let optionItemsList: any = [
        { label: "Cargas frigoríficas", value: 'A' },
        { label: "Cargas a granel", value: 'B' },
        { label: "Cargas vivas", value: 'C' },
        { label: "Cargas de grande porte", value: 'D' },
        { label: "Cargas de valor", value: 'E' },
        { label: "Cargas secas", value: 'F' },
        { label: "Cargas perigosas", value: 'G' },
        { label: "Cargas de minério e cimento", value: 'H' },
        { label: "Cargas frágeis", value: 'I' },
        { label: "Cargas de medicamentos", value: 'J' },
    ];

    let optionItems = optionItemsList.map((c: any) => {

        let active = activeA;
        if (c.value === 'B')
            active = activeB;
        else if (c.value === 'C')
            active = activeC;
        else if (c.value === 'D')
            active = activeD;
        else if (c.value === 'E')
            active = activeE;
        else if (c.value === 'F')
            active = activeF;
        else if (c.value === 'G')
            active = activeG;
        else if (c.value === 'H')
            active = activeH;
        else if (c.value === 'I')
            active = activeI;
        else if (c.value === 'J')
            active = activeJ;

        return <div className="option-item mt-3" key={optionItemsList.indexOf(c)}>
            <button className={`btn btn-outline btn-full ${active ? 'active' : ''}`} onClick={() => activeOption(c.value)}>{c.label}</button>
        </div>;
    });

    return (
        <>
            <Header step={5} total={7} title="Motoristas ou Motoboys" previous="#/perguntas-motorista-veiculo"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user">
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p>{'Caminhão'}</p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        <p>
                                            <Typewriter
                                                onInit={(typewriter) => {
                                                    typewriter
                                                        .changeDelay(15)
                                                        .typeString(`Qual tipo de <strong>carga você costuma transportar?</strong> <br>Pode escolher mais de uma.`)
                                                        .callFunction(() => {
                                                            setTyped(true)
                                                        })
                                                        .start();
                                                }} />
                                        </p>
                                    </div>
                                </div>

                                {typed &&
                                    <div className="option-list">
                                        {
                                            optionItems.map((objItem: any) => {
                                                return objItem;
                                            })
                                        }
                                    </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <footer className="footer-fixed">
                    <div className="container">
                        <div className="row d-flex align-items-center justify-content-center">
                            <div className="col-12 col-md-4">
                                <button className="btn btn-secondary btn-full" disabled={getCargo() === ''} onClick={() => setOption()}>Continuar</button>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

        </>);
}
