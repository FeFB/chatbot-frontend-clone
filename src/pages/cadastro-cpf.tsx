import { useEffect, useRef, useState } from "react";
import Header from "../components/header";
import InteractionModel, { InteractionAuthor } from "../data/InteractionModel";
import { SignupModelHelper } from "../data/SignupModel";
import Typewriter from 'typewriter-effect';
import ValidationService from "../services/PEXServices";
import TagManager from "react-gtm-module";
import { dataLayerHelper } from "../data/dataLayerModel";

export default function Cpf(props: any) {

    const [cpf, setDocument] = useState('');
    const [name, setName] = useState('');
    const [interactions, setInteractions] = useState<InteractionModel[]>([]);
    const [firstname, setFirstname] = useState('');
    const [typed, setTyped] = useState(false);
    const footerInput = useRef<HTMLInputElement>(null);

    useEffect(() => {
        setTyped(false);
        TagManager.dataLayer(dataLayerHelper.pagina('cpf', 'cadastro-usuario'));

        const signup = SignupModelHelper.get();
        if (signup) {
            setName(signup.name);
            setFirstname(signup.name.split(' ')[0]);
        } else {
            document.location = './';
        }
    }, [])

    useEffect(() => {
        if (typed) {
            footerInput?.current?.click();
            let chat = document.getElementsByClassName('chat')[0];
            chat.scrollTo(0, chat.scrollHeight);
        }
    }, [typed])

    function testaCPF(strCPF: string) {
        let Soma;
        let Resto;
        Soma = 0;
        if (strCPF === "00000000000") return false;
    
        for (let i=1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;
    
        if ((Resto === 10) || (Resto === 11))  Resto = 0;
        if (Resto !== parseInt(strCPF.substring(9, 10)) ) return false;
    
        Soma = 0;
        for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;
    
        if ((Resto === 10) || (Resto === 11))  Resto = 0;
        if (Resto !== parseInt(strCPF.substring(10, 11) ) ) return false;
        
        return true;
    }

    async function checkCPF() {
        setTyped(false)
        const cleanCpf = cpf.replaceAll(/[a-z ()\-.]/gi, '')

        if (cleanCpf.length !== 11) {
            const chat = [...interactions, {
                text: cpf,
                author: InteractionAuthor.CLIENTE
            }];
            setInteractions(chat);

            setTimeout(() => {
                setInteractions([...chat, {
                    author: InteractionAuthor.AMARELINHO,
                    text: 'Seu CPF deve ter 11 números (9 do número e 2 dígitos). Complete com zeros à esquerda, se necessário.'
                }])
                setTimeout(() => {
                    let chat = document.getElementsByClassName('chat')[0];
                    chat.scrollTo(0, chat.scrollHeight);
                }, 500);
            }, 1000);

        } else if (!testaCPF(cleanCpf)) {

            const chat = [...interactions, {
                text: cpf,
                author: InteractionAuthor.CLIENTE
            }];

            setInteractions(chat);

            setTimeout(() => {
                setInteractions([...chat, {
                    author: InteractionAuthor.AMARELINHO,
                    text: 'Este número não é de um CPF válido. Confira o número digitado e tente novamente.'
                }])
                setTimeout(() => {
                    let chat = document.getElementsByClassName('chat')[0];
                    chat.scrollTo(0, chat.scrollHeight);
                }, 500);
            }, 1000);

        } else {

            const ret = await ValidationService.validateCPF(cpf, name);

            if (!ret.success) {
                const chat = [...interactions, {
                    text: cpf,
                    author: InteractionAuthor.CLIENTE
                }];

                setInteractions(chat);

                if (ret.message?.indexOf('já está sendo utilizado') !== -1) {
                    ret.message += `&nbsp; Se você já possui cadastro no Amarelinho, tente fazer o <a href="/login.aspx">Login</a>.`;
                }

                setTimeout(() => {
                    setInteractions([...chat, {
                        author: InteractionAuthor.AMARELINHO,
                        text: ret.message ? ret.message : ''
                    }]);
                    setTimeout(() => {
                        let chat = document.getElementsByClassName('chat')[0];
                        chat.scrollTo(0, chat.scrollHeight);
                    }, 500);
                }, 1000);

            } else {

                SignupModelHelper.update({
                    document: cleanCpf,
                });
                document.location = '#/genero';
            }
        }
    }

    return (
        <>
            <Header step={3} total={17} title="Seu CPF" previous="#/nome"></Header>

            <div className="chat-container">

                <div className="chat">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-stretch justify-content-between">

                                <div className="chat-item chat-item-user" onClick={() => { footerInput?.current?.click(); alert(footerInput?.current) }}>
                                    <img src="images/ico-chat-user.svg" alt="Usuário" />
                                    <div className="chat-item-content">
                                        <p><i>{name}</i></p>
                                    </div>
                                </div>

                                <div className="chat-item">
                                    <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                    <div className="chat-item-content">
                                        { firstname &&
                                        <Typewriter
                                        onInit={(typewriter) => {
                                            typewriter.changeDelay(15).typeString(`Olá ${firstname}, <strong>informe seu CPF</strong>.<br/>
                                            E não se preocupe, ele só é utilizado para identificar você corretamente, não compartillhamos com os empregadores.`)
                                            .callFunction(() => {
                                                setTyped(true)
                                            })
                                            .start();
                                        }} />
                                        }
                                    </div>
                                </div>

                                {
                                    interactions.map((interaction) => {
                                        let ret;
                                        
                                        if (interaction.author === InteractionAuthor.AMARELINHO) {
                                            ret = <div className="chat-item" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-amarelinho.svg" alt="Amarelinho" />
                                                <div className="chat-item-content">
                                                    <Typewriter
                                                        onInit={(typewriter) => {
                                                            typewriter.changeDelay(15).typeString(interaction.text)
                                                            .callFunction(() => {
                                                                setTyped(true)
                                                            })
                                                            .start();
                                                        }} />
                                                </div>
                                            </div>

                                        } else if (interaction.author === InteractionAuthor.CLIENTE) {
                                            ret = <div className="chat-item chat-item-user" key={interactions.indexOf(interaction)}>
                                                <img src="images/ico-chat-user.svg" alt="Usuário" />
                                                <div className="chat-item-content">
                                                    <p>{interaction.text}</p>
                                                </div>
                                            </div>
                                        }
                                        return ret;
                                    })
                                }

                            </div>
                        </div>
                    </div>
                </div>
                                
                <footer className="footer-answer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="footer-answer-holder d-flex align-items-center justify-content-between">
                                    <input ref={footerInput} type="text" placeholder="Digite seu CPF" onChange={(e) => setDocument(e.target.value)} onKeyUp={(ev) => { if (ev.key === 'Enter') checkCPF() }} maxLength={14} />
                                    <button className="btn-submit" disabled={ cpf.length === 0 } onClick={() => checkCPF()}>
                                        <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.0114286 20L24 10L0.0114286 0L0 7.77778L17.1429 10L0 12.2222L0.0114286 20Z"></path></svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </>);
}
