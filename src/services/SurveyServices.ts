import { SignupModel, SignupModelHelper } from "../data/SignupModel";
import { ServiceResponse } from "./ServiceModels";

const axios = require('axios').create({
    baseURL: process.env.REACT_APP_SURVEY_API_URL
});

export default class SurveyServices {

    static async saveDriverQuestion(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const question = SignupModelHelper.toSurveyDriverQuestionModel(signup.driverQuestions[0]);
            const validation = await axios.patch('/api/resume', question);

            if (!validation.data.createdAt) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveDriverQuestion', ex });
            data = {
                message: 'Estou com problemas para salvar seus dados.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async saveDressmakerQuestion(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const question = SignupModelHelper.toSurveyDressmakerQuestionsModel(signup.dressmakerQuestions[0]);
            const validation = await axios.patch('/api/resume', question);

            if (!validation.data.createdAt) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveDriverQuestion', ex });
            data = {
                message: 'Estou com problemas para salvar seus dados.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async saveManicureQuestion(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const question = SignupModelHelper.toSurveyManicureQuestionsModel(signup.manicureQuestions[0]);
            const validation = await axios.patch('/api/resume', question);

            if (!validation.data.createdAt) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveDriverQuestion', ex });
            data = {
                message: 'Estou com problemas para salvar seus dados.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async savePainterQuestion(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const question = SignupModelHelper.toSurveyPainterQuestionsModel(signup.painterQuestions[0]);
            const validation = await axios.patch('/api/resume', question);

            if (!validation.data.createdAt) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveDriverQuestion', ex });
            data = {
                message: 'Estou com problemas para salvar seus dados.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async saveSellerQuestion(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const question = SignupModelHelper.toSurveySellerQuestionsModel(signup.sellerQuestions[0]);
            const validation = await axios.patch('/api/resume', question);

            if (!validation.data.createdAt) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveDriverQuestion', ex });
            data = {
                message: 'Estou com problemas para salvar seus dados.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

}
