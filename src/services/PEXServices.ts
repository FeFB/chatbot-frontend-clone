import { SignupModel, SignupModelHelper } from "../data/SignupModel";
import { ServiceResponse } from "./ServiceModels";

const axios = require('axios').create({
    baseURL: process.env.REACT_APP_API_URL
});

export default class PEXServices {
    static async validateEmail(email: string): Promise<ServiceResponse> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const validation = await axios.post('/servicos/seguranca.svc/validarEmail', {
                email
            });

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::validateEmail', ex });
            data = {
                message: 'Estou com problemas para validar seu e-mail.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async validateCPF(cpf: string, nome: string): Promise<ServiceResponse> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const validation = await axios.post('/servicos/seguranca.svc/validarCPF', {
                cpf,
                nome
            });

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::validateCPF', ex });
            data = {
                message: 'Estou com problemas para validar seu CPF.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async getAddressByZipCode(zip: string): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const validation = await axios.post('/servicos/perfil.svc/localizarCep', {
                cep: zip,
            });

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::getAddressByZipCode', ex });
            data = {
                message: 'Estou com problemas para validar seu CEP.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async saveAccount(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const validation = await axios.post('/servicos/seguranca.svc/cadastrar', SignupModelHelper.toPexModel(signup));

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveAccount', ex });
            data = {
                message: 'Estou com problemas para salvar seu cadastro.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }


    static async saveExperiences(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const experiences = SignupModelHelper.toPexExperienceModel(signup);
            const validation = await axios.post('/servicos/curriculo.svc/salvarExpProfissional', experiences);

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveExperiences', ex });
            data = {
                message: 'Estou com problemas para salvar seu cadastro.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }


    static async saveObjectives(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const objectives = SignupModelHelper.toPexObjectivesModel(signup);
            const validation = await axios.post('/servicos/curriculo.svc/salvarObjetivoProfissional', objectives);

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveObjectives', ex });
            data = {
                message: 'Estou com problemas para salvar seu cadastro.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async saveLanguages(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const objectives = SignupModelHelper.toPexLanguageModel(signup);
            const validation = await axios.post('/servicos/curriculo.svc/salvarIdioma', objectives);

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveLanguages', ex });
            data = {
                message: 'Estou com problemas para salvar seu cadastro.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async savePresentationLetter(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const letter = SignupModelHelper.toPexPresentationLetterModel(signup);
            const validation = await axios.post('/servicos/curriculo.svc/salvarCartaApresentacao', letter);

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveObjectives', ex });
            data = {
                message: 'Estou com problemas para salvar seu cadastro.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async saveGraduation(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const graduations = SignupModelHelper.toPexGraduationModel(signup);
            const validation = await axios.post('/servicos/curriculo.svc/salvarFormacaoEscolar', graduations);

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveGraduation', ex });
            data = {
                message: 'Estou com problemas para salvar seu cadastro.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

    static async saveCourses(signup: SignupModel): Promise<any> {
        let data: ServiceResponse = {
            success: true
        };

        try {
            const courses = SignupModelHelper.toPexCourseModel(signup);
            const validation = await axios.post('/servicos/curriculo.svc/salvarCurso', courses);

            if (!validation.data.sucesso) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    ...validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::saveCourses', ex });
            data = {
                message: 'Estou com problemas para salvar seu cadastro.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }


    static async isLogado(): Promise<any> {
        let data: any = {
            success: true
        };

        try {
            const logado = await axios.get('/servicos/seguranca.svc/Logado');
            if (logado.data.sucesso === true) {
                data = {
                    userId: logado.data.mensagem,
                    success: true
                }
            } else {
                data = {
                    message: logado.data.mensagemErro,
                    success: false
                };
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::isLogado', ex });
            data = {
                message: 'Usuário não está logado.',
                success: false
            };
        }

        return data;
    }

    static async getPositions(query: string): Promise<any> {
        let data: any = {
            success: true
        };

        try {
            const formData = new FormData();
            formData.append('q', query);
            const headers = {
                headers: {
                    "Content-Type": "multipart/form-data",
                },
            };

            const validation = await axios.post('/util/busca-cargos-principais.ashx', formData, headers);

            if (!validation.data) {
                data = {
                    message: validation.data.mensagemErro,
                    success: false
                };
            } else {
                data = {
                    items: validation.data,
                    success: true
                }
            }
        } catch (ex) {
            console.log({ location: 'ValidationService::getPositions', ex });
            data = {
                message: 'Estou com problemas para obter a lista de cargos.  Favor tente mais tarde.',
                success: false
            };
        }

        return data;
    }

}
