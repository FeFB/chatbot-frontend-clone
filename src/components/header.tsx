export default function Header(props: any) {

    const actives = [];
    const inactives = [];

    let x, y;
    for (x = 0; x < props.step; ++x) {
        actives.push(<span className="active" key={x}></span>);
    }

    for (y = x; y < props.total; ++y) {
        inactives.push(<span key={y}></span>);
    }

    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    window.addEventListener('resize', () => {
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    });

    return (
        <>
            <header className="header-curriculum">
                <div className="container">
                    <div className="row">
                        <div className="col-12 d-flex align-items-center justify-content-between">
                            <a href={props.previous} className="header-curriculum-back">
                                <img src="images/ico-arrow-left.svg" alt="Voltar" />
                            </a>
                            <p className="text-center">{props.title}</p>
                            {
                                props.finished &&
                                <img src="images/ico-concluded.svg" alt="Cadastro Finalizado" className="header-curriculum-finished"></img>
                            }
                        </div>
                    </div>
                </div>
                <div className="header-curriculum-steps d-flex align-items-end justify-content-between">
                    {actives}
                    {inactives}
                </div>
            </header>
        </>);
}
