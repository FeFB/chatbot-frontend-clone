export default interface InteractionModel {
    author: InteractionAuthor;
    text: string;
}

export enum InteractionAuthor {
    AMARELINHO = 1,
    CLIENTE = 2
}