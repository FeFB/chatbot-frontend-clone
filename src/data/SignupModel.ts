import PEXServices from "../services/PEXServices";

export interface SignupModel {
    name: string;
    gender: string;
    birthMonth: string;
    birthMonthNumber: number;
    birthDay: string;
    birthDayNumber: number;
    birthYear: string;
    birthYearNumber: number;
    phone: string;
    email: string;
    password: string;
    document: string;
    address: AddressModel;
    objectives: JobObjectiveModel[];
    graduations: GraduationModel[];
    experiences: JobExperiencesModel[];
    courses: CourseModel[];
    languages: LanguageModel[];
    presentationLetter: PresentationLetterModel;
    driverQuestions: DriverQuestionsModel[];
    dressmakerQuestions: DressmakerQuestionsModel[];
    manicureQuestions: ManicureQuestionsModel[];
    painterQuestions: PainterQuestionsModel[];
    sellerQuestions: SellerQuestionsModel[];
}

export interface PresentationLetterModel {
    description: string;
    saved: boolean;
}

export interface DriverQuestionsModel {
    workAtNight: boolean;
    workWeekend: boolean;
    license: string;
    vehicle: string;
    vehicleTrunk: boolean;
    vehicleCargo: string;
    vehicleMopp: boolean;
    vehicleYear: number;
    saved: boolean;
}

export interface DressmakerQuestionsModel {
    machine: string;
    cloth: string;
    saved: boolean;
}

export interface ManicureQuestionsModel {
    material: boolean;
    mei: boolean;
    technique: string;
    saved: boolean;
}

export interface PainterQuestionsModel {
    technique: string;
    saved: boolean;
}

export interface SellerQuestionsModel {
    type: string;
    saved: boolean;
}

export interface JobExperiencesModel {
    position: string;
    companyName: string;
    current: boolean;
    start: Date;
    finish: Date;
    activities: string;
    saved: boolean;
}

export interface AddressModel {
    zipCode: string;
    street: string;
    number: string;
    complement: string;
    neighborhood: string;
    city: string;
    state: string;
    typedStreet: boolean;
}

export interface GraduationModel {
    type: GraduationType;
    name: string;
    course: string;
    current: boolean;
    start: Date;
    finish: Date;
    saved: boolean;
}


export interface LanguageModel {
    type: LanguageType;
    level: LanguageLevel;
    otherType: string;
    saved: boolean;
}

export enum LanguageType {
    INGLES,
    ESPANHOL,
    FRANCES,
    ALEMAO,
    OUTRO
}

export enum LanguageLevel {
    BASICO,
    INTERMEDIARIO,
    FLUENTE,
    NATIVO
}

export interface CourseModel {
    type: CourseType;
    typeOther: string;
    name: string;
    course: string;
    current: boolean;
    start: Date;
    finish: Date;
    saved: boolean;
}

export enum CourseType {
    APRENDIZAGEM,
    CONCURSO,
    CURSO_LIVRE,
    GRADUACAO_E_POS,
    IDIOMA,
    TECNICO,
    OUTRO
}

export enum GraduationType {
    SEM_FORMACAO = 9,
    FUNDAMENTAL_INCOMPLETO = 7,
    FUNDAMENTAL_COMPLETO = 1,
    MEDIO_INCOMPLETO = 2,
    MEDIO_COMPLETO = 3,
    SUPERIOR_INCOMPLETO = 4,
    SUPERIOR_COMPLETO = 5,
    TECNICO = 8,
    POS_GRADUACAO = 6
}

export interface JobObjectiveModel {
    id: string;
    value: string;
    label: string;
}

export class JobObjectiveHelper {
    static fromList(l: any) {
        if (!l) return [];
        return l.map((item: any) => {
            return {
                id: item.id,
                value: item.value,
                label: item.label
            } as JobObjectiveModel;
        });
    }
}

export class SignupModelHelper {

    static getUserID(): String | null {
        let userId = localStorage.getItem('userId') || null;
        return userId;
    }
    
    static async checkLogin(): Promise<number | null> {
        let userId = localStorage.getItem('userId');
        if (!userId) {
            const req = await PEXServices.isLogado();
            if (req.success) {
                localStorage.setItem('userId', req.userId);
                userId = req.userId;
            } else {
                userId = null;
            }
        }
        return userId ? +userId : null;
    }

    static reset() {
        localStorage.setItem('signup', JSON.stringify(''));
    }

    static update(obj: any) {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);
            signupModel = { ...signupModel, ...obj };
        } else {
            signupModel = {
                ...obj
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
    }

    static getOngoingExperience(): JobExperiencesModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.experiences) {
                const latestExperience = signupModel.experiences.filter(experience => experience.saved === false);
                if (latestExperience.length === 1) {
                    return latestExperience[0];
                }
            }
        }
        return {
            saved: false,
        } as JobExperiencesModel;
    }

    static getOngoingGraduation(): GraduationModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.graduations) {
                const latestGraduation = signupModel.graduations.filter(graduation => graduation.saved === false);
                if (latestGraduation.length === 1) {
                    return latestGraduation[0];
                }
            }
        }
        return {
            saved: false,
        } as GraduationModel;
    }

    static getOngoingCourse(): CourseModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.courses) {
                const latestCourse = signupModel.courses.filter(course => course.saved === false);
                if (latestCourse.length === 1) {
                    return latestCourse[0];
                }
            }
        }
        return {
            saved: false,
        } as CourseModel;
    }

    static getOngoingLanguage(): LanguageModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.languages) {
                const latestLanguage = signupModel.languages.filter(course => course.saved === false);
                if (latestLanguage.length === 1) {
                    return latestLanguage[0];
                }
            }
        }
        return {
            saved: false,
        } as LanguageModel;
    }

    static getOngoingDriverQuestions(): DriverQuestionsModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.driverQuestions) {
                const latestDriverQuestion = signupModel.driverQuestions.filter(driverQuestion => driverQuestion.saved === false);
                if (latestDriverQuestion.length === 1) {
                    return latestDriverQuestion[0];
                }
            }
        }
        return {
            saved: false,
        } as DriverQuestionsModel;
    }

    static getOngoingDressmakerQuestions(): DressmakerQuestionsModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.dressmakerQuestions) {
                const latestQuestion = signupModel.dressmakerQuestions.filter(dressmakerQuestion => dressmakerQuestion.saved === false);
                if (latestQuestion.length === 1) {
                    return latestQuestion[0];
                }
            }
        }
        return {
            saved: false,
        } as DressmakerQuestionsModel;
    }

    static getOngoingManicureQuestions(): ManicureQuestionsModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.manicureQuestions) {
                const latestQuestion = signupModel.manicureQuestions.filter(manicureQuestion => manicureQuestion.saved === false);
                if (latestQuestion.length === 1) {
                    return latestQuestion[0];
                }
            }
        }
        return {
            saved: false,
        } as ManicureQuestionsModel;
    }

    static getOngoingPainterQuestions(): PainterQuestionsModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.painterQuestions) {
                const latestQuestion = signupModel.painterQuestions.filter(painterQuestion => painterQuestion.saved === false);
                if (latestQuestion.length === 1) {
                    return latestQuestion[0];
                }
            }
        }
        return {
            saved: false,
        } as PainterQuestionsModel;
    }

    static getOngoingSellerQuestions(): SellerQuestionsModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.sellerQuestions) {
                const latestQuestion = signupModel.sellerQuestions.filter(sellerQuestion => sellerQuestion.saved === false);
                if (latestQuestion.length === 1) {
                    return latestQuestion[0];
                }
            }
        }
        return {
            saved: false,
        } as SellerQuestionsModel;
    }

    static updateExperience(experiencePartialData: any): SignupModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.experiences) {
                const latestExperience = signupModel.experiences.filter(experience => experience.saved === false);
                if (latestExperience.length === 1) {
                    let idx = signupModel.experiences.indexOf(latestExperience[0]);
                    signupModel.experiences[idx] = {
                        ...signupModel.experiences[idx],
                        ...experiencePartialData
                    };
                } else {
                    signupModel.experiences.push({
                        saved: false,
                        ...experiencePartialData,
                    });
                }

            } else {
                signupModel.experiences = [{
                    saved: false,
                    ...experiencePartialData,
                }]
            }
        } else {
            var experience = {
                saved: false,
                ...experiencePartialData,
            } as JobExperiencesModel;
            signupModel = {
                experiences: [experience]
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
        return signupModel;
    }

    static updateGraduation(graduationPartialData: any): SignupModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.graduations) {
                const latestGraduation = signupModel.graduations.filter(graduation => graduation.saved === false);
                if (latestGraduation.length === 1) {
                    let idx = signupModel.graduations.indexOf(latestGraduation[0]);
                    signupModel.graduations[idx] = {
                        ...signupModel.graduations[idx],
                        ...graduationPartialData
                    };
                } else {
                    signupModel.graduations.push({
                        saved: false,
                        ...graduationPartialData,
                    });
                }

            } else {
                signupModel.graduations = [{
                    saved: false,
                    ...graduationPartialData,
                }]
            }
        } else {
            var graduation = {
                saved: false,
                ...graduationPartialData,
            } as GraduationModel;
            signupModel = {
                graduations: [graduation]
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
        return signupModel;
    }

    static updateCourse(coursePartialData: any): SignupModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.courses) {
                const latestCourse = signupModel.courses.filter(course => course.saved === false);
                if (latestCourse.length === 1) {
                    let idx = signupModel.courses.indexOf(latestCourse[0]);
                    signupModel.courses[idx] = {
                        ...signupModel.courses[idx],
                        ...coursePartialData
                    };
                } else {
                    signupModel.courses.push({
                        saved: false,
                        ...coursePartialData,
                    });
                }

            } else {
                signupModel.courses = [{
                    saved: false,
                    ...coursePartialData,
                }]
            }
        } else {
            var course = {
                saved: false,
                ...coursePartialData,
            } as CourseModel;
            signupModel = {
                courses: [course]
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
        return signupModel;
    }

    static updateLanguage(languagePartialData: any): SignupModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.languages) {
                const latestLanguage = signupModel.languages.filter(language => language.saved === false);
                if (latestLanguage.length === 1) {
                    let idx = signupModel.languages.indexOf(latestLanguage[0]);
                    signupModel.languages[idx] = {
                        ...signupModel.languages[idx],
                        ...languagePartialData
                    };
                } else {
                    signupModel.languages.push({
                        saved: false,
                        ...languagePartialData,
                    });
                }

            } else {
                signupModel.languages = [{
                    saved: false,
                    ...languagePartialData,
                }]
            }
        } else {
            var language = {
                saved: false,
                ...languagePartialData,
            } as LanguageModel;
            signupModel = {
                languages: [language]
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
        return signupModel;
    }

    static updateDriverQuestions(driverPartialData: any): SignupModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.driverQuestions) {
                const latestQuestion = signupModel.driverQuestions.filter(driverQuestion => driverQuestion.saved === false);
                if (latestQuestion.length === 1) {
                    let idx = signupModel.driverQuestions.indexOf(latestQuestion[0]);
                    signupModel.driverQuestions[idx] = {
                        ...signupModel.driverQuestions[idx],
                        ...driverPartialData
                    };
                } else {
                    signupModel.driverQuestions.push({
                        saved: false,
                        ...driverPartialData,
                    });
                }

            } else {
                signupModel.driverQuestions = [{
                    saved: false,
                    ...driverPartialData,
                }]
            }
        } else {
            var driverQuestions = {
                saved: false,
                ...driverPartialData,
            } as DriverQuestionsModel;
            signupModel = {
                driverQuestions: [driverQuestions]
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
        return signupModel;
    }

    static updateDressmakerQuestions(dressmakerPartialData: any): SignupModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.dressmakerQuestions) {
                const latestQuestion = signupModel.dressmakerQuestions.filter(dressmakerQuestion => dressmakerQuestion.saved === false);
                if (latestQuestion.length === 1) {
                    let idx = signupModel.dressmakerQuestions.indexOf(latestQuestion[0]);
                    signupModel.dressmakerQuestions[idx] = {
                        ...signupModel.dressmakerQuestions[idx],
                        ...dressmakerPartialData
                    };
                } else {
                    signupModel.dressmakerQuestions.push({
                        saved: false,
                        ...dressmakerPartialData,
                    });
                }

            } else {
                signupModel.dressmakerQuestions = [{
                    saved: false,
                    ...dressmakerPartialData,
                }]
            }
        } else {
            var dressmakerQuestions = {
                saved: false,
                ...dressmakerPartialData,
            } as DressmakerQuestionsModel;
            signupModel = {
                dressmakerQuestions: [dressmakerQuestions]
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
        return signupModel;
    }

    static updateManicureQuestions(manicurePartialData: any): SignupModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.manicureQuestions) {
                const latestQuestion = signupModel.manicureQuestions.filter(manicureQuestion => manicureQuestion.saved === false);
                if (latestQuestion.length === 1) {
                    let idx = signupModel.manicureQuestions.indexOf(latestQuestion[0]);
                    signupModel.manicureQuestions[idx] = {
                        ...signupModel.manicureQuestions[idx],
                        ...manicurePartialData
                    };
                } else {
                    signupModel.manicureQuestions.push({
                        saved: false,
                        ...manicurePartialData,
                    });
                }

            } else {
                signupModel.manicureQuestions = [{
                    saved: false,
                    ...manicurePartialData,
                }]
            }
        } else {
            var manicureQuestions = {
                saved: false,
                ...manicurePartialData,
            } as ManicureQuestionsModel;
            signupModel = {
                manicureQuestions: [manicureQuestions]
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
        return signupModel;
    }

    static updatePainterQuestions(painterPartialData: any): SignupModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.painterQuestions) {
                const latestQuestion = signupModel.painterQuestions.filter(painterQuestion => painterQuestion.saved === false);
                if (latestQuestion.length === 1) {
                    let idx = signupModel.painterQuestions.indexOf(latestQuestion[0]);
                    signupModel.painterQuestions[idx] = {
                        ...signupModel.painterQuestions[idx],
                        ...painterPartialData
                    };
                } else {
                    signupModel.painterQuestions.push({
                        saved: false,
                        ...painterPartialData,
                    });
                }

            } else {
                signupModel.painterQuestions = [{
                    saved: false,
                    ...painterPartialData,
                }]
            }
        } else {
            var painterQuestions = {
                saved: false,
                ...painterPartialData,
            } as PainterQuestionsModel;
            signupModel = {
                painterQuestions: [painterQuestions]
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
        return signupModel;
    }

    static updateSellerQuestions(sellerPartialData: any): SignupModel {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel;
        if (signup) {
            signupModel = JSON.parse(signup);

            if (signupModel.sellerQuestions) {
                const latestQuestion = signupModel.sellerQuestions.filter(sellerQuestion => sellerQuestion.saved === false);
                if (latestQuestion.length === 1) {
                    let idx = signupModel.sellerQuestions.indexOf(latestQuestion[0]);
                    signupModel.sellerQuestions[idx] = {
                        ...signupModel.sellerQuestions[idx],
                        ...sellerPartialData
                    };
                } else {
                    signupModel.sellerQuestions.push({
                        saved: false,
                        ...sellerPartialData,
                    });
                }

            } else {
                signupModel.sellerQuestions = [{
                    saved: false,
                    ...sellerPartialData,
                }]
            }
        } else {
            var sellerQuestions = {
                saved: false,
                ...sellerPartialData,
            } as SellerQuestionsModel;
            signupModel = {
                sellerQuestions: [sellerQuestions]
            } as SignupModel;
        }
        localStorage.setItem('signup', JSON.stringify(signupModel));
        return signupModel;
    }

    static get(): SignupModel | null {
        const signup = localStorage.getItem('signup');
        let signupModel: SignupModel | null = null;
        if (signup) {
            signupModel = JSON.parse(signup);
        }
        return signupModel;
    }

    static toPexExperienceModel(model: SignupModel): any {
        function isValidExperience(xp: JobExperiencesModel): boolean {
            return xp.position !== null && xp.activities !== null && xp.companyName !== null && xp.saved === true && xp.start !== null;
        }

        return {
            listCurriculoExperienciaProfissional: model.experiences.filter(ex => isValidExperience(ex)).map(obj => {
                const dtStart = new Date(obj.start);
                const dataAdmissao = `${String(dtStart.getDate()).padStart(2, '0')}/${String(dtStart.getMonth() + 1).padStart(2, '0')}/${dtStart.getFullYear()}`;

                let dataDesligamento;
                if (obj.finish) {
                    const dtFinish = new Date(obj.finish);
                    dataDesligamento = `${String(dtFinish.getDate()).padStart(2, '0')}/${String(dtFinish.getMonth() + 1).padStart(2, '0')}/${dtFinish.getFullYear()}`;
                } else {
                    dataDesligamento = null;
                }

                const objPEX = {
                    atribuicoes: obj.activities,
                    cargo: obj.position,
                    codigo: '',
                    dataAdmissao,
                    dataDesligamento,
                    empresaNome: obj.companyName
                };

                return objPEX;
            })
        };
    }

    static toPexGraduationModel(model: SignupModel): any {
        function isValidGraduation(graduation: GraduationModel): boolean {
            return graduation.name !== null && graduation.saved === true && graduation.start !== null;
        }

        return {
            listCurriculoFormacaoEscolar: model.graduations.filter(graduation => isValidGraduation(graduation)).map(graduation => {
                let dtStart = new Date(graduation.start);
                if (graduation.type === GraduationType.SEM_FORMACAO) {
                    graduation.start = new Date(2000, 0, 1, 0, 0, 0, 0);
                }

                const dataInicio = `${String(dtStart.getDate()).padStart(2, '0')}/${String(dtStart.getMonth() + 1).padStart(2, '0')}/${dtStart.getFullYear()}`;

                let dataConclusao;
                if (graduation.finish) {
                    const dtFinish = new Date(graduation.finish);
                    dataConclusao = `${String(dtFinish.getDate()).padStart(2, '0')}/${String(dtFinish.getMonth() + 1).padStart(2, '0')}/${dtFinish.getFullYear()}`;
                } else {
                    dataConclusao = null;
                }


                const objPEX = {
                    grauFormacaoEscolar: graduation.type.toString(),
                    nomeCurso: graduation.course,
                    nomeInstituicao: graduation.name,
                    codigo: '',
                    dataInicio,
                    dataConclusao,
                };

                return objPEX;
            })
        };
    }

    static toPexCourseModel(model: SignupModel): any {
        function isValidCourse(course: CourseModel): boolean {
            return course.name !== null && course.saved === true && course.start !== null;
        }

        function getCourseType(type: CourseType) {
            switch (type) {
                case CourseType.APRENDIZAGEM: return "Aprendizagem";
                case CourseType.CONCURSO: return "Concurso";
                case CourseType.CURSO_LIVRE: return "Curso livre";
                case CourseType.GRADUACAO_E_POS: return "Graduação e pós";
                case CourseType.IDIOMA: return "Idioma";
                case CourseType.TECNICO: return "Técnico";
                case CourseType.OUTRO: return "Outros";
            }
        }

        return {
            listCurriculoCurso: model.courses.filter(course => isValidCourse(course)).map(course => {
                const dtStart = new Date(course.start);
                const dataInicio = `${String(dtStart.getDate()).padStart(2, '0')}/${String(dtStart.getMonth() + 1).padStart(2, '0')}/${dtStart.getFullYear()}`;

                let dataConclusao;
                if (course.finish) {
                    const dtFinish = new Date(course.finish);
                    dataConclusao = `${String(dtFinish.getDate()).padStart(2, '0')}/${String(dtFinish.getMonth() + 1).padStart(2, '0')}/${dtFinish.getFullYear()}`;
                } else {
                    dataConclusao = null;
                }

                if (course.type === CourseType.OUTRO) {
                    course.course = `${course.typeOther} - ${course.course}`
                }

                const objPEX = {
                    cursoTipo: getCourseType(course.type),
                    nomeCurso: course.course,
                    instituicao: course.name,
                    dataInicio,
                    dataConclusao,
                    edMais: "{@edMais}"
                };

                return objPEX;
            })
        };
    }

    static toPexLanguageModel(model: SignupModel): any {
        function isValidLanguage(language: LanguageModel): boolean {
            return language.level !== null && language.type !== null && language.saved === true;
        }

        function getLanguageType(type: LanguageType, other: string) {
            if (other) return other;
            switch (type) {
                case LanguageType.ALEMAO: return "5";
                case LanguageType.ESPANHOL: return "3";
                case LanguageType.FRANCES: return "4";
                case LanguageType.INGLES: return "1";
                case LanguageType.OUTRO: return "8";
            }
        }

        function getLanguageLevel(level: LanguageLevel): string {
            switch (level) {
                case LanguageLevel.BASICO: return "01";
                case LanguageLevel.FLUENTE: return "03";
                case LanguageLevel.INTERMEDIARIO: return "02";
                case LanguageLevel.NATIVO: return "04";
            }
        }

        return {
            listCurriculoIdioma: model.languages.filter(language => isValidLanguage(language)).map(language => {
                const objPEX = {
                    idioma: getLanguageType(language.type, language.otherType),
                    nivel: getLanguageLevel(language.level),
                };

                return objPEX;
            })
        };
    }

    static toPexObjectivesModel(model: SignupModel): any {
        return {
            listObjetivoProfissional: model.objectives.map(obj => obj.value)
        };
    }

    static toPexPresentationLetterModel(model: SignupModel): any {
        return {
            cartaApresentacao: model.presentationLetter.description
        };
    }

    static toPexModel(model: SignupModel): any {
        function maiusculaPrimeiraLetrar(valor: string): string {
            return valor[0].toUpperCase() + valor.slice(1);
        }

        function getSexo(gender: string): string {
            let genderCode;
            switch (gender) {
                case 'Feminino':
                    genderCode = 'FE';
                    break;
                case 'Masculino':
                    genderCode = 'MA';
                    break;
                default:
                    genderCode = 'ND';
                    break;
            }
            return genderCode;
        }

        return {
            nome: maiusculaPrimeiraLetrar(model.name.trim().split(' ')[0]),
            sobrenome: maiusculaPrimeiraLetrar(model.name.trim().substring(model.name.trim().indexOf(' ') + 1)),
            cpf: model.document,
            dataNascimento: new Date(model.birthYearNumber, model.birthMonthNumber - 1, model.birthDayNumber),
            celularDDD: model.phone.substring(0, 2),
            celularNumero: model.phone.substring(2),
            enderecoCep: model.address.zipCode,
            enderecoLogradouro: model.address.street,
            enderecoNumero: model.address.number,
            enderecoComplemento: model.address.complement,
            enderecoBairro: model.address.neighborhood,
            enderecoCidade: model.address.city,
            enderecoEstado: model.address.state,
            enderecoEstadoCidadeCodigo: null,
            sexo: getSexo(model.gender),
            email: model.email,
            senha: model.password,
            integracaoSocial: null,
            idSocial: null
        };
    }


    static toSurveyDriverQuestionModel(question: DriverQuestionsModel): any {

        return {
            externalId: SignupModelHelper.getUserID(),
            data: {
                workAtNight: question.workAtNight,
                workWeekend: question.workWeekend,
                license: question.license,
                vehicle: question.vehicle,
                vehicleTrunk: question.vehicleTrunk,
                vehicleCargo: question.vehicleCargo,
                vehicleMopp: question.vehicleMopp,
                vehicleYear: question.vehicleYear
            }
        };
    }

    static toSurveyDressmakerQuestionsModel(question: DressmakerQuestionsModel): any {

        return {
            externalId: SignupModelHelper.getUserID(),
            data: {
                machine: question.machine,
                cloth: question.cloth
            }
        };
    }

    static toSurveyManicureQuestionsModel(question: ManicureQuestionsModel): any {

        return {
            externalId: SignupModelHelper.getUserID(),
            data: {
                material: question.material,
                mei: question.mei,
                technique: question.technique,
            }
        };
    }

    static toSurveyPainterQuestionsModel(question: PainterQuestionsModel): any {

        return {
            externalId: SignupModelHelper.getUserID(),
            data: {
                technique: question.technique,
            }
        };
    }

    static toSurveySellerQuestionsModel(question: SellerQuestionsModel): any {

        return {
            externalId: SignupModelHelper.getUserID(),
            data: {
                type: question.type,
            }
        };
    }
}