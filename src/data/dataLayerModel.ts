import { SignupModelHelper } from "./SignupModel";

export interface dataLayerPaginaObjectModel {
    event: string;
    userid: string;
    passo: string;
    tipo: string;
}

export interface dataLayerFormEnviadoObjectModel {
    event: string;
    userid: string;
    tipo_form: string;
}

export interface dataLayerMeasureClickObjectModel {
    event: string;
    category: string;
    action: string;
    label: string;
}

export interface dataLayerPaginaModel {
    dataLayer: dataLayerPaginaObjectModel;
}

export interface dataLayerFormEnviadoModel {
    dataLayer: dataLayerFormEnviadoObjectModel;
}

export interface dataLayerMeasureClickModel {
    dataLayer: dataLayerMeasureClickObjectModel;
}

export class dataLayerHelper {

    static pagina(passo: string, tipo: string): dataLayerPaginaModel {

        const userid = SignupModelHelper.getUserID();
        
        return {
            dataLayer: {
                userid,
                passo,
                tipo,
                event: 'pagina',
            } as dataLayerPaginaObjectModel
        } as dataLayerPaginaModel
    }

    static formEnviado(tipo_form: string): dataLayerFormEnviadoModel {

        const userid = SignupModelHelper.getUserID();

        return {
            dataLayer: {
                userid,
                tipo_form,
                event: 'form_enviado',
            } as dataLayerFormEnviadoObjectModel
        } as dataLayerFormEnviadoModel
    }

    static measureClick(action: string, label: string, category: string = 'cadastro-usuario'): dataLayerMeasureClickModel {

        return {
            dataLayer: {
                event: 'measureClick',
                category: '',
                action,
                label
            } as dataLayerMeasureClickObjectModel
        } as dataLayerMeasureClickModel
    }
}