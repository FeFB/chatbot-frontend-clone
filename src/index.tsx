import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {
  HashRouter,
  Route,
  Routes,
} from "react-router-dom";

import Login from './pages/cadastro-login';
import Intro from './pages/cadastro-intro';
import Nome from './pages/cadastro-nome';
import Genero from './pages/cadastro-genero';
import NascimentoMes from './pages/cadastro-nascimento-mes';
import NascimentoAno from './pages/cadastro-nascimento-ano';
import NascimentoDia from './pages/cadastro-nascimento-dia';
import Celular from './pages/cadastro-celular';
import Email from './pages/cadastro-email';
import Senha from './pages/cadastro-senha';
import Cpf from './pages/cadastro-cpf';
import Finalizado from './pages/cadastro-finalizado-botoes';
import FinalizadoFinal from './pages/cadastro-finalizado-final';
import FinalizadoAindaNao from './pages/cadastro-finalizado-ainda-nao';
import Termos from './pages/cadastro-termos';
import Cep from './pages/cadastro-cep';
import EnderecoNumero from './pages/cadastro-endereco-numero';
import EnderecoComplemento from './pages/cadastro-endereco-complemento';
import Cidade from './pages/cidade';
import CidadePesquisa from './pages/cidade-pesquisa';
import ObjetivosProfissionais from './pages/objetivos-profissionais';
import TagManager from 'react-gtm-module';
import ObjetivosProfissionaisPesquisa from './pages/objetivos-profissionais-pesquisa';
import ExperienciaPossuiNao from './pages/experiencia-possui-nao';
import ExperienciaCargo from './pages/experiencia-cargo';
import ExperienciasProfissionais from './pages/experiencias-profissionais';
import ExperienciaEmpresa from './pages/experiencia-empresa';
import ExperienciaAtividades from './pages/experiencia-atividades';
import ExperienciaAindaTrabalha from './pages/experiencia-ainda-trabalha';
import ExperienciaEntradaAno from './pages/experiencia-entrada-ano';
import ExperienciaEntradaMes from './pages/experiencia-entrada-mes';
import ExperienciaSaidaAno from './pages/experiencia-saida-ano';
import ExperienciaSaidaMes from './pages/experiencia-saida-mes';
import ExperienciaAdicionarOutra from './pages/experiencia-adicionar-outra';
import EscolaridadeInicio from './pages/escolaridade-inicio';
import EscolaridadeCurso from './pages/escolaridade-curso';
import EscolaridadeInstituicao from './pages/escolaridade-instituicao';
import EscolaridadeAindaEstuda from './pages/escolaridade-ainda-estuda';
import EscolaridadeInicioAno from './pages/escolaridade-inicio-ano';
import EscolaridadeInicioMes from './pages/escolaridade-inicio-mes';
import EscolaridadeFimMes from './pages/escolaridade-fim-mes';
import EscolaridadeFimAno from './pages/escolaridade-fim-ano';
import CursosInicio from './pages/cursos-inicio';
import CursosPossuiNao from './pages/cursos-possui-nao';
import CursosTipo from './pages/cursos-tipo';
import CursosOutro from './pages/cursos-outro';
import CursosInstituicao from './pages/cursos-instituicao';
import CursosCurso from './pages/cursos-curso';
import CursosAindaEstuda from './pages/cursos-ainda-estuda';
import CursosInicioAno from './pages/cursos-inicio-ano';
import CursosInicioMes from './pages/cursos-inicio-mes';
import IdiomasInicio from './pages/idiomas-inicio';
import IdiomasApenasPortugues from './pages/idiomas-apenas-portugues';
import IdiomasOutro from './pages/idiomas-outro';
import IdiomasNivel from './pages/idiomas-nivel';
import IdiomasAdicionarOutro from './pages/idiomas-adicionar-outro';
import CartaApresentacao from './pages/carta-apresentacao';
import PerguntasMotoristaTrabalharNoite from './pages/perguntas-motorista-trabalhar-noite';
import PerguntasMotoristaTrabalharFinalDeSemana from './pages/perguntas-motorista-trabalhar-finaldesemana';
import PerguntasMotoristaHabilitacao from './pages/perguntas-motorista-habilitacao';
import PerguntasMotoristaVeiculo from './pages/perguntas-motorista-veiculo';
import PerguntasMotoristaVeiculoNaoPossuo from './pages/perguntas-motorista-veiculo-naopossuo';
import PerguntasMotoristaVeiculoAno from './pages/perguntas-motorista-veiculo-ano';
import PerguntasMotoristaVeiculoBau from './pages/perguntas-motorista-veiculo-bau';
import PerguntasMotoristaVeiculoMopp from './pages/perguntas-motorista-veiculo-mopp';
import PerguntasMotoristaVeiculoCarga from './pages/perguntas-motorista-veiculo-carga';
import PerguntasPintorTecnica from './pages/perguntas-pintor-tecnicas';
import PerguntasVendedorTipo from './pages/perguntas-vendedor-tipo';
import PerguntasCostureiraMaquina from './pages/perguntas-costureira-maquina';
import PerguntasCostureiraTecido from './pages/perguntas-costureira-tecido';
import PerguntasManicureTecnica from './pages/perguntas-manicure-tecnicas';
import PerguntasManicureMei from './pages/perguntas-manicure-mei';
import PerguntasManicureMaterial from './pages/perguntas-manicure-material';
import CursosFimAno from './pages/cursos-fim-ano';
import CursosFimMes from './pages/cursos-fim-mes';
import EnderecoLogradouro from './pages/cadastro-endereco-logradouro';
import EnderecoBairro from './pages/cadastro-endereco-bairro';


const tagManagerArgs = {
  gtmId: 'GTM-NJCFN8F'
}
TagManager.initialize(tagManagerArgs);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <HashRouter>
    <Routes>
      <Route path="/" element={<Intro />}></Route>
      <Route path="/login" element={<Login />}></Route>
      <Route path="/nome" element={<Nome />}></Route>
      <Route path="/genero" element={<Genero />}></Route>
      <Route path="/nascimento-mes" element={<NascimentoMes />}></Route>
      <Route path="/nascimento-ano" element={<NascimentoAno />}></Route>
      <Route path="/nascimento-dia" element={<NascimentoDia />}></Route>
      <Route path="/celular" element={<Celular />}></Route>
      <Route path="/email" element={<Email />}></Route>
      <Route path="/senha" element={<Senha />}></Route>
      <Route path="/cpf" element={<Cpf />}></Route>
      <Route path="/cep" element={<Cep />}></Route>
      <Route path="/endereco-logradouro" element={<EnderecoLogradouro />}></Route>
      <Route path="/endereco-numero" element={<EnderecoNumero />}></Route>
      <Route path="/endereco-complemento" element={<EnderecoComplemento />}></Route>
      <Route path="/endereco-bairro" element={<EnderecoBairro />}></Route>
      <Route path="/cidade" element={<Cidade />}></Route>
      <Route path="/cidade-pesquisa" element={<CidadePesquisa />}></Route>
      <Route path="/termos" element={<Termos />}></Route>
      <Route path="/objetivos-profissionais" element={<ObjetivosProfissionais />}></Route>
      <Route path="/objetivos-profissionais-pesquisa" element={<ObjetivosProfissionaisPesquisa />}></Route>
      <Route path="/finalizado" element={<Finalizado />}></Route>
      <Route path="/finalizado-final" element={<FinalizadoFinal />}></Route>
      <Route path="/finalizado-ainda-nao" element={<FinalizadoAindaNao />}></Route>
      <Route path="/experiencias-profissionais" element={<ExperienciasProfissionais />}></Route>
      <Route path="/experiencia-possui-nao" element={<ExperienciaPossuiNao />}></Route>
      <Route path="/experiencia-cargo" element={<ExperienciaCargo />}></Route>
      <Route path="/experiencia-empresa" element={<ExperienciaEmpresa />}></Route>
      <Route path="/experiencia-atividades" element={<ExperienciaAtividades />}></Route>
      <Route path="/experiencia-ainda-trabalha" element={<ExperienciaAindaTrabalha />}></Route>
      <Route path="/experiencia-entrada-ano" element={<ExperienciaEntradaAno />}></Route>
      <Route path="/experiencia-saida-ano" element={<ExperienciaSaidaAno />}></Route>
      <Route path="/experiencia-entrada-mes" element={<ExperienciaEntradaMes />}></Route>
      <Route path="/experiencia-saida-mes" element={<ExperienciaSaidaMes />}></Route>
      <Route path="/experiencia-adicionar-outra" element={<ExperienciaAdicionarOutra />}></Route>
      <Route path="/escolaridade-inicio" element={<EscolaridadeInicio />}></Route>
      <Route path="/escolaridade-curso" element={<EscolaridadeCurso />}></Route>
      <Route path="/escolaridade-instituicao" element={<EscolaridadeInstituicao />}></Route>
      <Route path="/escolaridade-ainda-estuda" element={<EscolaridadeAindaEstuda />}></Route>
      <Route path="/escolaridade-inicio-ano" element={<EscolaridadeInicioAno />}></Route>
      <Route path="/escolaridade-inicio-mes" element={<EscolaridadeInicioMes />}></Route>
      <Route path="/escolaridade-fim-ano" element={<EscolaridadeFimAno />}></Route>
      <Route path="/escolaridade-fim-mes" element={<EscolaridadeFimMes />}></Route>
      <Route path="/cursos-inicio" element={<CursosInicio />}></Route>
      <Route path="/cursos-possui-nao" element={<CursosPossuiNao />}></Route>
      <Route path="/cursos-tipo" element={<CursosTipo />}></Route>
      <Route path="/cursos-outro" element={<CursosOutro />}></Route>
      <Route path="/cursos-instituicao" element={<CursosInstituicao />}></Route>
      <Route path="/cursos-curso" element={<CursosCurso />}></Route>
      <Route path="/cursos-ainda-estuda" element={<CursosAindaEstuda />}></Route>
      <Route path="/cursos-inicio-ano" element={<CursosInicioAno />}></Route>
      <Route path="/cursos-inicio-mes" element={<CursosInicioMes />}></Route>
      <Route path="/cursos-fim-ano" element={<CursosFimAno />}></Route>
      <Route path="/cursos-fim-mes" element={<CursosFimMes />}></Route>
      <Route path="/idiomas-inicio" element={<IdiomasInicio />}></Route>
      <Route path="/idiomas-apenas-portugues" element={<IdiomasApenasPortugues />}></Route>
      <Route path="/idiomas-outro" element={<IdiomasOutro />}></Route>
      <Route path="/idiomas-nivel" element={<IdiomasNivel />}></Route>
      <Route path="/idiomas-adicionar-outro" element={<IdiomasAdicionarOutro />}></Route>
      <Route path="/carta-apresentacao" element={<CartaApresentacao />}></Route>
      <Route path="/perguntas-costureira-maquina" element={<PerguntasCostureiraMaquina />}></Route>
      <Route path="/perguntas-costureira-tecido" element={<PerguntasCostureiraTecido />}></Route>
      <Route path="/perguntas-manicure-tecnica" element={<PerguntasManicureTecnica />}></Route>
      <Route path="/perguntas-manicure-mei" element={<PerguntasManicureMei />}></Route>
      <Route path="/perguntas-manicure-material" element={<PerguntasManicureMaterial />}></Route>
      <Route path="/perguntas-motorista-trabalhar-noite" element={<PerguntasMotoristaTrabalharNoite />}></Route>
      <Route path="/perguntas-motorista-trabalhar-finaldesemana" element={<PerguntasMotoristaTrabalharFinalDeSemana />}></Route>
      <Route path="/perguntas-motorista-habilitacao" element={<PerguntasMotoristaHabilitacao />}></Route>
      <Route path="/perguntas-motorista-veiculo" element={<PerguntasMotoristaVeiculo />}></Route>
      <Route path="/perguntas-motorista-veiculo-naopossuo" element={<PerguntasMotoristaVeiculoNaoPossuo />}></Route>
      <Route path="/perguntas-motorista-veiculo-bau" element={<PerguntasMotoristaVeiculoBau />}></Route>
      <Route path="/perguntas-motorista-veiculo-carga" element={<PerguntasMotoristaVeiculoCarga />}></Route>
      <Route path="/perguntas-motorista-veiculo-mopp" element={<PerguntasMotoristaVeiculoMopp />}></Route>
      <Route path="/perguntas-motorista-veiculo-ano" element={<PerguntasMotoristaVeiculoAno />}></Route>
      <Route path="/perguntas-pintor-tecnica" element={<PerguntasPintorTecnica />}></Route>
      <Route path="/perguntas-vendedor-tipo" element={<PerguntasVendedorTipo />}></Route>
    </Routes>
  </HashRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
